//
//  RLUIFontExtensions.swift
//  Rally
//
//  Created by Jayakrishnan S on 10/10/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    class func regular(_ fontSize: CGFloat = 15) -> UIFont {
        return UIFont(name: "SFUIText-Regular", size: fontSize)!
    }
    
    class func bold(_ fontSize: CGFloat = 12) -> UIFont {
        return UIFont(name: "SFUIText-Bold", size: fontSize)!
    }
    
    class func medium(_ fontSize: CGFloat = 10) -> UIFont {
        return UIFont(name: "SFUIText-Medium", size: fontSize)!
    }
    
    class func light(_ fontSize: CGFloat = 9) -> UIFont {
        return UIFont(name: "SFUIText-Light", size: fontSize)!
    }
    
    class func semibold(_ fontSize: CGFloat = 9) -> UIFont {
        return UIFont(name: "SFUIText-Semibold", size: fontSize)!
    }
    
    class func heavy(_ fontSize: CGFloat = 9) -> UIFont {
        return UIFont(name: "SFUIText-Heavy", size: fontSize)!
    }
}
