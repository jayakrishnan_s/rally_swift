//
//  UIViewController.swift
//  Rally
//
//  Created by Jayakrishnan S on 08/11/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import UIKit
import MBProgressHUD


extension UIViewController{
    func showScreen(_ storyBoardName:String, identifier:String, isModel:Bool){
        
    }
    
    func adaptToBackgroundTransparencyOverPresentation() {
        
        self.modalPresentationStyle = .overCurrentContext
        self.providesPresentationContextTransitionStyle = true
        self.definesPresentationContext = true
    }
}

extension UIViewController{
    func ShowLoader(withText txt:String?){
        MBProgressHUD.hide(for: ScreenManager.sharedInstance.getTopViewController().view, animated: true)
        let hud = MBProgressHUD()
        hud.label.text = txt
        view.addSubview(hud)
        hud.center = view.center
        hud.show(animated: true)
    }
    
    func HideLoader(){
        MBProgressHUD.hide(for: view, animated: true)
    }

}
