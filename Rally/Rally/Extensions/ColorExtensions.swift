//
//  ColorExtensions.swift
//  Rally
//
//  Created by Jayakrishnan S on 10/10/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(red: CGFloat, green: CGFloat, blue: CGFloat) {
        self.init(red: red / 255, green: green / 255, blue: blue / 255, alpha: 1)
    }
    
    func hex() -> UInt {
        
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        var colorAsUInt: UInt = 0
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            colorAsUInt = UInt(red * 255.0) << 16
            colorAsUInt += UInt(green * 255.0) << 8
            colorAsUInt += UInt(blue * 255.0)
        }
        return colorAsUInt
    }
    
    class func from(rgb hexValue: UInt) -> UIColor {
        return UIColor(red: CGFloat((hexValue & 0xFF0000) >> 16) / 255.0,
                       green: CGFloat((hexValue & 0x00FF00) >> 8) / 255.0,
                       blue: CGFloat((hexValue & 0x0000FF) >> 0) / 255.0, alpha: 1.0)
    }
}

struct ColorPalette {
    static let AppKeyColor = UIColor(red: 60, green: 93, blue: 150)
    static let AppRedColor = UIColor(red: 181, green: 13, blue: 36)

    static let signUpBtnColor = UIColor.from(rgb: 0x335b8b)
    static let colorForDecline = UIColor(red: 251, green: 64, blue: 83)
    
    static let hyperlink = UIColor.from(rgb: 0xfdff63)
    static let loginCaption = UIColor.from(rgb: 0xbbdaac)
    static let grayColor = UIColor.from(rgb: 0x7d929c)
    static let Bluish = UIColor.from(rgb: 0x4e6e7d)
    static let greenish = UIColor.from(rgb: 0x71D1C7)
    static let white = UIColor.from(rgb: 0xffffff)
    static let red = UIColor.from(rgb: 0xff3b3a)
    static let offWhite = UIColor.from(rgb: 0xE8E8E8)
    static let lightGray = UIColor.from(rgb: 0xaaaaaa)//D0D0D0
    static let userGray = UIColor.from(rgb: 0xa5abae)
    static let timeBluish = UIColor.from(rgb: 0x263e49)
    static let green = UIColor.from(rgb: 0x48ac2d)
    static let black = UIColor.from(rgb: 0x000000)
    static let unitColor = UIColor.from(rgb: 0x656565)
    static let eeeaea = UIColor.from(rgb: 0xeeeaea)
    static let lightBluish = UIColor.from(rgb: 0x67747a)
    static let grayBluish = UIColor.from(rgb: 0x81a2b2)
    static let seperatorLine = UIColor.from(rgb: 0xdde1e3)
    static let verticalSeperatorLine = UIColor.from(rgb: 0xf4f4f4)
    static let setAvailabilitySeperator = UIColor.from(rgb: 0xe2e2e2)
    
    static let chatBackground = UIColor.from(rgb: 0xF6F6F6)
    static let chatNameColor = UIColor.from(rgb: 0x7b909a)
    static let chatDateColor = UIColor.from(rgb: 0x9aa9b1)
    static let cardBackground = UIColor.from(rgb: 0xf5f5f7)
    static let moreButtonBlue = UIColor.from(rgb: 0x30b1a4)
    static let moreButtonBG = UIColor.from(rgb: 0xf1f1f1)
    static let doneButtonGreen = UIColor.from(rgb: 0x8fcd5f)
    
    static let color58c44a = UIColor.from(rgb: 0x58c44a)
    static let chatButtonTextColor = UIColor.from(rgb: 0x58c44a)
    static let chatGreyBgColor = UIColor.from(rgb: 0xf8f8f8) // normal screen BG
    static let headerBgColor = UIColor.from(rgb: 0xc6cbc8)
    
    static let addressLabelColor = UIColor.from(rgb: 0x7599aa)
    static let greenButtonColor = UIColor.from(rgb: 0x7ED0B9)
    static let gradientGray = UIColor.from(rgb: 0xd7d7d7)
    
    static let gradientGreen = UIColor.from(rgb: 0x91cc5a)
    static let gradientBlue = UIColor.from(rgb: 0x5ed8c2)
    
    static let calendarBlue = UIColor.from(rgb: 0x58d9cd)
    static let calendarTitle = UIColor.from(rgb: 0x2e343b)
    static let calendarDate = UIColor.from(rgb: 0x8d99a6)
    static let calendarOtherMonthDate = UIColor.from(rgb: 0xc7ccd1)
    static let calendarNotesBG = UIColor.from(rgb: 0xf2fbfb)
    static let calendarNotesHeaderBG = UIColor.from(rgb: 0xebfadd)
    static let calendarNotes = UIColor.from(rgb: 0x5f6875)
    static let calendarNotesSelectedBG = UIColor.from(rgb: 0xe1f5ff)
    static let calendarTime = UIColor.from(rgb: 0x707070)
    static let viewBorder = UIColor.from(rgb: 0xe1e1e1)
    static let placeHolder = UIColor.from(rgb: 0xd6d7d8)
    
    static let dayButton = UIColor.from(rgb: 0x5b5b5b)
    static let dayButtonBG = UIColor.from(rgb: 0xd8d8d8)
    static let inviteeBG = UIColor.from(rgb: 0xececec)
    static let darkGrey = UIColor.from(rgb: 0xd0d0d0)
    static let orangeYellow = UIColor.from(rgb: 0xffb427)
    
}
