//
//  String+Rally.swift
//  Rally
//
//  Created by Jayakrishnan S on 31/03/18.
//  Copyright © 2018 Jayakrishnan.S. All rights reserved.
//

import Foundation
extension String{
    
    private func getDateFromGeneral(format:String,dateString:String)->Date?{
        let formatter = DateFormatter()
        formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        formatter.dateFormat = format
        return formatter.date(from: dateString)
    }

    func getDate() -> Date?{
        let datesArray = DateFormat_General.map{ getDateFromGeneral(format:$0,dateString:self)}.filter{$0 != nil}
        guard datesArray.count>0 else{
            return nil
        }
        return datesArray.first!
    }

}
