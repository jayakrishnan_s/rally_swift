//
//  UIImageView+Rally.swift
//  Rally
//
//  Created by Jayakrishnan S on 27/03/18.
//  Copyright © 2018 Jayakrishnan.S. All rights reserved.
//

import Foundation
import SDWebImage

extension UIImageView{
    
    func setUrl(imageUrl:URL){
        self.image = UIImage(named: "name")
        self.sd_setImage(with: imageUrl) { (image, error, cacheType, url) in
            if let image = image{
                self.image = image
            }
        }
    }
}
