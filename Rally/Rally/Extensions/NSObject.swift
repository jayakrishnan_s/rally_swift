//
//  NSObject.swift
//  Rally
//
//  Created by Jayakrishnan S on 08/11/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import Foundation

extension NSObject{
    func doAfter(time : Double, _ block: @escaping ()->Void){
        DispatchQueue.main.asyncAfter(deadline: .now() + time) {
            // your code here
            block()
        }
    }
}
