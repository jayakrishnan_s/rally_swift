//
//  Date+Rally.swift
//  Rally
//
//  Created by Jayakrishnan S on 31/03/18.
//  Copyright © 2018 Jayakrishnan.S. All rights reserved.
//

import Foundation

extension Date{

    func universalFormattedDate() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = universalDateFormat
        formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        return formatter.string(from: self)
    }
    
    func timeAgo(numericDates:Bool) -> String {
        let date = self
        let calendar = NSCalendar.current


        let unitFlags:Set<Calendar.Component> = [ .minute , .hour, .day, .weekOfYear, .month, .year, .second]
        let now = Date()
        let earliest = now.compare(date) == .orderedAscending ? now : self
        let latest = (earliest == now as Date) ? date : now
        let components = calendar.dateComponents(unitFlags, from: earliest, to: latest)
        if let year = components.year, (year >= 2) {
            return "\(year) years ago"
        } else if let year = components.year, (year >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if let month = components.month, (month >= 2) {
            return "\(month) months ago"
        } else if let month = components.month, (month >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if  let weekOfYear = components.weekOfYear, (weekOfYear >= 2) {
            return "\(weekOfYear) weeks ago"
        } else if let weekOfYear = components.weekOfYear, (weekOfYear >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if let day = components.day, (day >= 2) {
            return "\(day) days ago"
        } else if let day = components.day,(day >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if let hour = components.hour,(hour >= 2) {
            return "\(hour) hours ago"
        } else if let hour = components.hour, (hour >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if let minute = components.minute, (minute >= 2) {
            return "\(minute) minutes ago"
        } else if let minute = components.minute,(minute >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if let second = components.second,(second >= 3) {
            return "\(second) seconds ago"
        } else {
            return "Just now"
        }
    }
}
