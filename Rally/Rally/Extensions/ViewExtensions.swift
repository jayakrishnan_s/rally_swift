//
//  ViewExtensions.swift
//  Rally
//
//  Created by Jayakrishnan S on 10/10/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import UIKit

let cornerRadiusVal: CGFloat = 10.0

extension UIView {
    
    //    class func fromNib<T: UIView>() -> T {
    //        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as T
    //    }
    //
    func makeItRound() {
        layer.cornerRadius = bounds.size.height / 2
        clipsToBounds = true
    }
    
    func roundMyCorners(radius: CGFloat = cornerRadiusVal, borderColor: UIColor = ColorPalette.offWhite) {
        layer.borderWidth = 1.0
        layer.cornerRadius = radius
        layer.borderColor = borderColor.cgColor
        layer.masksToBounds = true
    }
    
    func makeShadow(color: UIColor, radius: CGFloat) {
        
        layer.shadowColor = color.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.masksToBounds = false
        layer.shadowRadius = radius
        layer.shadowOpacity = 0.5
    }
    
    func makeGradientBorder(from color1: UIColor, to color2: UIColor, with width: CGFloat = 3) {
        
        let gradient = CAGradientLayer()
        gradient.frame =  CGRect(origin: .zero, size: frame.size)
        gradient.colors = [color1.cgColor, color2.cgColor]
        
        layer.cornerRadius = frame.size.width / 2
        clipsToBounds = true
        
        let radius = bounds.size.height / 2
        let shape = CAShapeLayer()
        shape.lineWidth = width
        shape.path = UIBezierPath(arcCenter: CGPoint(x: radius, y: radius), radius: CGFloat(radius), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true).cgPath
        shape.strokeColor = UIColor.black.cgColor
        shape.fillColor = UIColor.clear.cgColor
        gradient.mask = shape
        
        self.layer.addSublayer(gradient)
    }
    
    func addDip(with xTranslation: CGFloat) {
        
        self.removeDip()
        let image = UIImage(named: "rt-chat-image-dip")
        let dipView = UIImageView(image: image)
        dipView.tag = 100
        
        dipView.frame = CGRect(origin: CGPoint(x: xTranslation, y: self.frame.origin.y), size: dipView.frame.size)
        self.addSubview(dipView)
        
    }
    
    func removeDip() {
        if let dipView = self.viewWithTag(100) {
            dipView.removeFromSuperview()
        }
    }
    
    @discardableResult func addRightBorder(color: UIColor, width: CGFloat) -> UIView {
        let layer = CALayer()
        layer.borderColor = color.cgColor
        layer.borderWidth = width
        layer.frame = CGRect(x: self.frame.size.width-width, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(layer)
        return self
    }
    @discardableResult func addLeftBorder(color: UIColor, width: CGFloat) -> UIView {
        let layer = CALayer()
        layer.borderColor = color.cgColor
        layer.borderWidth = width
        layer.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(layer)
        return self
    }
    @discardableResult func addTopBorder(color: UIColor, width: CGFloat) -> UIView {
        let layer = CALayer()
        layer.borderColor = color.cgColor
        layer.borderWidth = width
        layer.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        self.layer.addSublayer(layer)
        return self
    }
    @discardableResult func addBottomBorder(color: UIColor, width: CGFloat) -> UIView {
        let layer = CALayer()
        layer.borderColor = color.cgColor
        layer.borderWidth = width
        layer.frame = CGRect(x: 0, y: self.frame.size.height-width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(layer)
        return self
    }
}

class ShadowView: UIView {
    
    var shadowOffset = CGSize(width: 0, height: 0)
    var shadowColor = ColorPalette.lightGray.cgColor
    var backColor = UIColor.clear
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundColor = backColor
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 3.0
        
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadiusVal).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        
    }
}
