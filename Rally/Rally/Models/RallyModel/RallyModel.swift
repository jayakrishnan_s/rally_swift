//
//  RallyModel.swift
//  Rally
//
//  Created by Jayakrishnan S on 28/02/18.
//  Copyright © 2018 Jayakrishnan.S. All rights reserved.
//

import Foundation
import GoogleSignIn

class RallyModel {
    
    init() {
        
    }
    
    var eventId = "\(arc4random())"
    var eventTitle = "No Title"
    var createdBy :  RallyUserModel?
    var eventDate:Date?
    var addedTime : Date?
    var eventDescription = ""
    var locationName = ""
    var location:CLLocation?
    var startTime:Date?
    var endTime:Date?
    var hours:String?
    var members:[RallyUser]?
    var eventProfilePic:UIImage?
    
    convenience init(with managedObjectModel:Event) {
        self.init()

        if let Id = managedObjectModel.eventId{
            self.eventId = Id
        }
        eventTitle = managedObjectModel.title ?? ""
        eventDescription = managedObjectModel.eventDescription ?? ""
        startTime = managedObjectModel.startTime?.getDate()
        endTime = managedObjectModel.endTime?.getDate()
        
        locationName = managedObjectModel.locationName ?? ""
        
        location = CLLocation(latitude: managedObjectModel.latitude, longitude: managedObjectModel.longitude)
        addedTime = managedObjectModel.addedDate?.getDate()
        hours = managedObjectModel.hours
        eventDate = managedObjectModel.eventDate?.getDate()
        if let parent = managedObjectModel.parent{
            createdBy =  RallyUserModel(with: parent)
        }

        if let data = managedObjectModel.eventProfilePic{
            eventProfilePic = UIImage(data: data)
        }
        if let members = managedObjectModel.members{
            if nil == self.members{
                self.members = [RallyUser]()
            }
            members.forEach({ (user) in
                if let userModel = user as? RallyUser {
                    self.members?.append(userModel)
                }
            })
        }

    }
    
    func updateToCoreData(){
        guard let managedObjectModel = Event.fetchObject(withAttributeName: "eventId", attributeValue: self.eventId) else {
            if let context = CoreDataManager.sharedInstance.managedObjectContext{
                let managedObjectModel = Event(context: context)
                managedObjectModel.eventId = "\(eventId)"
                managedObjectModel.title = eventTitle
                managedObjectModel.eventDescription = eventDescription
                managedObjectModel.startTime = startTime?.toWeekDayAndDateString()
                managedObjectModel.endTime = startTime?.universalFormattedDate()

                managedObjectModel.locationName = locationName
                if let location = location{
                    managedObjectModel.latitude = location.coordinate.latitude
                    managedObjectModel.longitude = location.coordinate.longitude
                }
                managedObjectModel.addedDate = addedTime?.universalFormattedDate()
                managedObjectModel.hours = hours
                managedObjectModel.eventDate = eventDate?.universalFormattedDate()
                managedObjectModel.parent = createdBy?.getManagedObjectModel()
                if let image = eventProfilePic, let data = UIImageJPEGRepresentation(image, 1){
                    managedObjectModel.eventProfilePic = data
                }
                if let _ = members{
                    managedObjectModel.members = NSSet(array: members!)
                }
                context.saveMe()
            }
            return
        }
        managedObjectModel.title = eventTitle
        managedObjectModel.eventDescription = eventDescription
        managedObjectModel.startTime = startTime?.universalFormattedDate()
        managedObjectModel.endTime = startTime?.universalFormattedDate()
        
        managedObjectModel.locationName = locationName
        if let location = location{
            managedObjectModel.latitude = location.coordinate.latitude
            managedObjectModel.longitude = location.coordinate.longitude
        }
        managedObjectModel.addedDate = addedTime?.universalFormattedDate()
        managedObjectModel.hours = hours
        managedObjectModel.eventDate = eventDate?.universalFormattedDate()
        managedObjectModel.parent = createdBy?.getManagedObjectModel()
        if let image = eventProfilePic, let data = UIImageJPEGRepresentation(image, 1){
            managedObjectModel.eventProfilePic = data
        }
        if let _ = members{
            managedObjectModel.members = NSSet(array: members!)
        }
        if let context = CoreDataManager.sharedInstance.managedObjectContext{
            context.saveMe()
        }
    }
}

class RallyUserModel{
    init() {
        
    }
    
    convenience init(withGoogleData user:GIDGoogleUser) {
        self.init()
        
        googleUserId = user.userID                  // For client-side use only!
        googleAuthToken = user.authentication.idToken // Safe to send to the server
//        let fullName = user.profile.name
        firstName = user.profile.givenName
        lastName = user.profile.familyName
        email = user.profile.email
        profilePic = user.profile.imageURL(withDimension: 55)
        updateToCoreData()
    }
    
    convenience init(with managedObjectModel:RallyUser) {
        self.init()
        firstName = managedObjectModel.firstName
        lastName = managedObjectModel.lastName
        email = managedObjectModel.email ?? ""
        if let _ = managedObjectModel.profilePic {
            profilePic =  URL(string: managedObjectModel.profilePic!)
        }
    }
    
    private func updateToCoreData(){
        guard let savedModel = RallyUser.fetchObject(withAttributeName: "email", attributeValue: email) else {
            if let context = CoreDataManager.sharedInstance.managedObjectContext{
                let managedObjectModel = RallyUser(context: context)
                managedObjectModel.firstName = firstName
                managedObjectModel.lastName = lastName
                managedObjectModel.email = email
                managedObjectModel.profilePic = profilePic?.absoluteString
                context.saveMe()
            }
            return
        }
        savedModel.firstName = firstName
        savedModel.lastName = lastName
        savedModel.email = email
        savedModel.profilePic = profilePic?.absoluteString
        if let context = CoreDataManager.sharedInstance.managedObjectContext{
            context.saveMe()
        }
    }
    
    func getManagedObjectModel()->RallyUser?{
        return RallyUser.fetchObject(withAttributeName: "email", attributeValue: email)
    }
    
    convenience init(withFacebookData details:NSDictionary) {
        self.init()
        
        firstName = details["first_name"] as? String
        lastName = details["last_name"] as? String
        email = (details["email"] as? String) ?? ""
        facebookUserId = details["id"] as? String
        if let urlDetails = details["picture"] as? [String: Any],let data =  urlDetails["data"] as? [String: Any],let urlString = data["url"] as? String,let url = URL(string: urlString){
            profilePic = url
        }
        
        updateToCoreData()
    }

    var rallyId:String?
    var firstName:String?
    var lastName:String?
    var profilePic : URL?
    var phoneNumber = ""
    var email = ""
    var googleAuthToken:String?
    var googleUserId:String?
    var facebookUserId:String?
}

