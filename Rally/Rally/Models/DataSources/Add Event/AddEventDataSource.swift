//
//  AddEventDataSource.swift
//  Rally
//
//  Created by Jayakrishnan S on 26/12/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import Foundation
import UIKit

let AENameCell = "AENameCell"
let AEDescriptionCell = "AEDescriptionCell"
let AEButtonCell = "AEButtonCell"
let AEHourCell = "AEHourCell"

class AddEventDataSource : NSObject, UITableViewDataSource {
    
    override init() {
        super.init()
    }
    
    private var locationText:String = "Location"
    private var image:UIImage?
    private var membersText:String = "Friends"
    private var durationText:String = ""

    func setText(text:String, _ buttontype:AEButtonType?, fieldType:AEFieldType? = nil, image:UIImage?){
        if let type = buttontype{
            switch type {
            case .Add_Location:
                locationText = text
            case .Add_Contacts:
                membersText = text
            default:
                self.image = image
            }
        }
        
        if let field = fieldType, field == .AEDuration {
            durationText = text
        }
    }
    
    var cellDelegate:AECellDelegate?
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = getCellForIndexPath(indexPath, tableview: tableView) else{
            return UITableViewCell()
        }
        cell.delegate = cellDelegate
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    private func getCellForIndexPath(_ path: IndexPath, tableview:UITableView)-> AddEventBaseCell?{
        var cell : AddEventBaseCell?
        
        switch path.row {
        case 0:
            cell = tableview.dequeueReusableCell(withIdentifier: AENameCell, for: path) as? AddEventBaseCell
        case 1:
            cell = tableview.dequeueReusableCell(withIdentifier: AEDescriptionCell) as? AddEventBaseCell
        case 2:
            if let buttonCell =  tableview.dequeueReusableCell(withIdentifier: AEButtonCell) as? AddEventButtonCell{
                buttonCell.buttontype = .Add_Contacts
                buttonCell.lblTitle?.text = membersText
                cell = buttonCell
            }
            
        case 3:
            if let hourCell =  tableview.dequeueReusableCell(withIdentifier: AEHourCell) as? AddEventDurationCell{
                hourCell.txtHours?.text = durationText
                cell = hourCell
            }
        case 4:
            if let buttonCell =  tableview.dequeueReusableCell(withIdentifier: AEButtonCell) as? AddEventButtonCell{
                buttonCell.buttontype = .Add_Location
                buttonCell.lblTitle?.text = locationText
                cell = buttonCell
            }
        case 5:
            if let buttonCell =  tableview.dequeueReusableCell(withIdentifier: AEButtonCell) as? AddEventButtonCell{
                buttonCell.buttontype = .Add_Image
                if image != nil{
                    buttonCell.eventImageView?.isHidden = false
                    buttonCell.eventImageView?.image = image
                    buttonCell.lblTitle?.isHidden = true
                }
                cell = buttonCell
            }

        default:
            cell = AddEventBaseCell()
        }
        return cell
    }
    
}
