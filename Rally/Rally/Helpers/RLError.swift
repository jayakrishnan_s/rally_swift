//
//  RLError.swift
//  Rally
//
//  Created by Jayakrishnan S on 10/10/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol RLErrorProtocol: Error {

    var localizedTitle: String { get }
    var localizedDescription: String { get }
    var code: Int { get }
}

struct RLError: RLErrorProtocol {
    
    var localizedTitle: String
    var localizedDescription: String
    var code: Int
    
    init(localizedTitle: String?, localizedDescription: String, code: Int) {
        self.localizedTitle = localizedTitle ?? "Error"
        self.localizedDescription = localizedDescription
        self.code = code
    }
}
