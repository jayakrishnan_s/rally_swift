//
//  ImageManager.swift
//  Rally
//
//  Created by Jayakrishnan S on 21/03/18.
//  Copyright © 2018 Jayakrishnan.S. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Photos

enum CameraType:Int {
    
    case front
    case rear
}

enum CameraMode:Int {
    
    case video
    case photo
}

enum SourceType:Int {
    
    case camera
    case photoLibrary
}

class ImagePickerSettings:NSObject {
    
    var cameraMode:CameraMode = .photo
    var sourceType:SourceType = .photoLibrary
    var cameraType:CameraType = .rear
    var isEditable:Bool = false
}

protocol ImagePickerCallBack {
    
    func imagePickerDidFinishPickingImage(image:UIImage)
    func imagePickerDidCancel()
}

class ProfileImageHelper: NSObject {
    
    let kGR_Tag_Alert_ImagePicker = 90890
    
    fileprivate var imagePicker:UIImagePickerController? = nil
    
    fileprivate var cameraSettings:ImagePickerSettings? = nil
    fileprivate var imagePickerCallBack:ImagePickerCallBack? = nil
    fileprivate weak var controllerRef:UIViewController? = nil
    fileprivate var actionSheetHelper:ActionSheetHelper? = nil
    var userAuthorized: Bool = false
    
    func showImagePickerWithSourceOptions(FromController controller:UIViewController, withSettings settings:ImagePickerSettings, delegate ref:ImagePickerCallBack)
    {
        self.cameraSettings = settings
        self.controllerRef = controller
        self.imagePickerCallBack = ref
        
        self.actionSheetHelper = ActionSheetHelper()
        self.actionSheetHelper?.controllerRef = controller
        self.actionSheetHelper?.tag = kGR_Tag_Alert_ImagePicker
        self.actionSheetHelper?.delegate = self
        
        let camera = ActionOption(optionText: "Take a Picture")
        let photoLibrary = ActionOption(optionText: "Choose from Photo Library")
        let cancel = ActionOption(optionText: "Cancel", optionType: OptionType.cancel)
        
        userAuthorized = false
        
        self.actionSheetHelper?.showMessage(" ", withTitle: " ", messageOptions: [camera, photoLibrary, cancel], messageType: ActionType.action)
    }
    
    func showImagePicker(FromController controller:UIViewController, withSettings settings:ImagePickerSettings, delegate ref:ImagePickerCallBack) {
        
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        
        switch cameraAuthorizationStatus {
        case .authorized:
            userAuthorized = true
            self.cameraSettings = settings
            self.imagePickerCallBack = ref
            self.imagePicker = UIImagePickerController()
            self.imagePicker?.sourceType = ( settings.sourceType == .camera ) ? UIImagePickerControllerSourceType.camera : UIImagePickerControllerSourceType.photoLibrary
            
            switch settings.sourceType {
                
            case .camera:
                
                self.imagePicker?.cameraCaptureMode = ( settings.cameraMode == .photo ) ? UIImagePickerControllerCameraCaptureMode.photo : UIImagePickerControllerCameraCaptureMode.video
                
                if let cameraType = self.cameraSettings?.cameraType
                {
                    self.imagePicker?.cameraDevice = ( cameraType == .front ) ? .front : .rear
                }
                break
            default:
                break
            }
            
            self.imagePicker?.allowsEditing = settings.isEditable
            self.imagePicker?.delegate = self
            
            controller.present(self.imagePicker!, animated: true, completion: nil)
            break
        case .restricted:
            self.cameraSettings = settings
            self.imagePickerCallBack = ref
            
            self.imagePicker = UIImagePickerController()
            self.imagePicker?.sourceType = ( settings.sourceType == .camera ) ? UIImagePickerControllerSourceType.camera : UIImagePickerControllerSourceType.photoLibrary
            
            switch settings.sourceType {
                
            case .camera:
                
                self.imagePicker?.cameraCaptureMode = ( settings.cameraMode == .photo ) ? UIImagePickerControllerCameraCaptureMode.photo : UIImagePickerControllerCameraCaptureMode.video
                
                if let cameraType = self.cameraSettings?.cameraType {
                    self.imagePicker?.cameraDevice = ( cameraType == .front ) ? .front : .rear
                }
                break
                
            default:
                break
            }
            
            userAuthorized = true
            self.imagePicker?.allowsEditing = settings.isEditable
            self.imagePicker?.delegate = self
            
            controller.present(self.imagePicker!, animated: true, completion: nil)
            
            break
            
        case .notDetermined:
            
            if settings.sourceType == .camera {
                AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
                    if granted == true {
                        
                        self.imagePicker = UIImagePickerController()
                        self.imagePicker?.sourceType = ( settings.sourceType == .camera ) ? UIImagePickerControllerSourceType.camera : UIImagePickerControllerSourceType.photoLibrary
                        self.imagePicker?.allowsEditing = settings.isEditable
                        self.imagePicker?.delegate = self
                        self.userAuthorized = true
                        
                        DispatchQueue.main.async {
                            controller.present(self.imagePicker!, animated: true, completion: nil)
                        }
                        
                    } else {
                        return
                    }
                })
            }else{
                PHPhotoLibrary.requestAuthorization({status in
                    if status == .authorized{
                        
                        self.imagePicker = UIImagePickerController()
                        self.imagePicker?.sourceType = ( settings.sourceType == .camera ) ? UIImagePickerControllerSourceType.camera : UIImagePickerControllerSourceType.photoLibrary
                        self.imagePicker?.allowsEditing = settings.isEditable
                        self.imagePicker?.delegate = self
                        self.userAuthorized = true
                        
                        DispatchQueue.main.async {
                            controller.present(self.imagePicker!, animated: true, completion: nil)
                        }
                    } else { return }
                })
            }
            break
        case .denied :
            
            self.userAuthorized = false
            if settings.sourceType == .camera{
                Common.showSimpleAlert(message: "Camera access permission request denied. You can change the camera access permission from settings.", controller: ScreenManager.sharedInstance.getTopViewController())
            }else{
                Common.showSimpleAlert(message: "Photo library access permission request denied. You can change the Photo library access permission from settings.", controller: ScreenManager.sharedInstance.getTopViewController())
            }
            break
            // Prompting user for the permission to use the camera.
        }
    }
}

extension ProfileImageHelper:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.imagePickerCallBack?.imagePickerDidCancel()
        self.dismissAndReset(picker)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if (!self.userAuthorized) {
            return
        }
        
        if let settings = self.cameraSettings, settings.cameraMode == .photo {
            if let pickedImage = ( settings.isEditable ? info[UIImagePickerControllerEditedImage] : info[UIImagePickerControllerOriginalImage] ) as? UIImage {
                self.imagePickerCallBack?.imagePickerDidFinishPickingImage(image: pickedImage)
            }
        }
        self.dismissAndReset(picker)
    }
    
    private func dismissAndReset(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        self.imagePicker = nil
        self.controllerRef = nil
        self.imagePickerCallBack = nil
        self.cameraSettings = nil
    }
}

//MARK:- Message alert delegate methods

extension ProfileImageHelper: ActionHelperDelegate
{
    func messengerOptionTapped(_ option: ActionOption, inMessenger messenger: ActionSheetHelper) {
        
        if actionSheetHelper?.tag == kGR_Tag_Alert_ImagePicker
        {
            switch option.index {
                
            case 0:
                self.cameraSettings?.sourceType = .camera
                self.showImagePicker(FromController: self.controllerRef!, withSettings: self.cameraSettings!, delegate: self.imagePickerCallBack!)
                break
                
            case 1:
                self.cameraSettings?.sourceType = .photoLibrary
                self.showImagePicker(FromController: self.controllerRef!, withSettings: self.cameraSettings!, delegate: self.imagePickerCallBack!)
                break
                
            default:
                self.actionSheetHelper?.dismiss()
                self.actionSheetHelper = nil
                
                break
            }
        }
    }
}
