//
//  Constants.swift
//  Rally
//
//  Created by Jayakrishnan S on 10/10/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import Foundation

let GooglePlacesAPI_Key = "AIzaSyBvym6tfvck_HbpFP0iV3HcrURLIP_vkFo"
let AppName = "Rally"

enum storyBoard:String {
    case splash = "Splash"
    case login = "Login"
    case main = "Main"
}

let universalDateFormat = "MM/dd/yyy hh:mm:ss a"
let DateFormat_General  = ["dd-MM-yyy HH:mm:ss","MM/dd/yyy hh:mm:ss a","dd MMM yyyy HH:mm:ss","yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ", "EEE dd MMMM,yyyy", "HH:mm", "MM-dd-yyyy HH:mm"]

