//
//  Preferance.swift
//  FuseMap
//
//  Created by Jayakrishnan S on 07/12/16.
//  Copyright © 2016  jk. All rights reserved.
//

import UIKit

let keyUUID = "uuid"

let keySessionToken = "sessiontoken"
let keyDeviceToken = "devicetoken"
let keyAPI_KEY = "API_KEY"
let keyEmail = "email"
let keyPIN  = "pin"
let keyPassword = "password"
let keyIsAutologin = "isAutologin"

class Preferance: NSObject {
    
    let defaults = UserDefaults.standard
    
    class func saveUUID(_ id:String){
        UserDefaults.standard.set(id , forKey: keyUUID)
        UserDefaults.standard.synchronize()
    }
    
    class func getUUID()-> String?{
        
        guard let val = UserDefaults.standard.object(forKey: keyUUID) as? String else{
            let uuid = UUID().uuidString
            Preferance.saveUUID(uuid)
            return uuid
        }
        return val
    }
    
    // MARK:  SessionToken
    class func saveSessionToken(token tok:String){
        //keySessionToken
        
        UserDefaults.standard.set(tok , forKey: keySessionToken)
        UserDefaults.standard.synchronize()
    }
    
    // MARK:  SessionToken
    class func saveDeviceToken(token tok:String){
        //keySessionToken
        
        UserDefaults.standard.set(tok , forKey: keyDeviceToken)
        UserDefaults.standard.synchronize()
    }

    class func getDeviceToken()->String?{
        
        guard let sessionToken = UserDefaults.standard.object(forKey: keyDeviceToken) as? String else{
            print("\nNo device token available")
            Common.sharedCommon().writeLog("\nNo device token available")
            return "dummy"
        }
        return sessionToken
    }

    class func getSessionToken()->String?{
        
        guard let sessionToken = UserDefaults.standard.object(forKey: keySessionToken) as? String else{
            return nil
        }
        return sessionToken
    }
    
    // MARK:  API_KEY
    class func saveAPI_Key(_ key:String){
        //keySessionToken
        
        UserDefaults.standard.set(key , forKey: keyAPI_KEY)
        UserDefaults.standard.synchronize()
    }
    
    class func getAPI_Key()->String?{
        
        guard let key = UserDefaults.standard.object(forKey: keyAPI_KEY) as? String else{
            return nil //"6d9f729b765aaecabo5e5ef9150fa073f8a61b94"
        }
        return key
    }
    
    class func saveUser(userObject user:UserModel, isAutologin:Bool){
        if let email = user.email, let password = user.password{
            UserDefaults.standard.set(email, forKey: keyEmail)
            UserDefaults.standard.set(password, forKey: keyPassword)
        }
        UserDefaults.standard.set(user.accessToken, forKey: keySessionToken)
        UserDefaults.standard.set(user.address, forKey: "address")
        UserDefaults.standard.set(user.status, forKey: "status")
        UserDefaults.standard.set(user.gender, forKey: "gender")
        UserDefaults.standard.set(user.phoneNo, forKey: "phone")
        UserDefaults.standard.set(user.firstName, forKey: "fname")
        UserDefaults.standard.set(user.lastName, forKey: "lname")
        UserDefaults.standard.set(user.dob, forKey: "dob")
        UserDefaults.standard.set(user.image, forKey: "image")
        UserDefaults.standard.set(user.loginDate, forKey: "logindate")
        UserDefaults.standard.set(user.dateCreated, forKey: "datecreated")
        UserDefaults.standard.synchronize()
    }
    
    class func getSavedUser()-> UserModel?{
        
        let user = UserModel()
        if let email = UserDefaults.standard.value(forKey: keyEmail), let password = UserDefaults.standard.value(forKey: keyPassword){
            user.email = email as? String
            user.password = password as? String
        }
        
        user.accessToken =  UserDefaults.standard.value(forKey: keySessionToken) as? String
        user.address =  UserDefaults.standard.value(forKey: "address") as? String
        user.brandCategoryID =  UserDefaults.standard.value( forKey: "barandcatid") as? String
        user.status =  UserDefaults.standard.value( forKey: "status") as? String
        user.gender =  UserDefaults.standard.value(forKey: "gender") as? String
        user.phoneNo =  UserDefaults.standard.value(forKey: "phone") as? String
        
        user.stateID =  UserDefaults.standard.value( forKey: "stateid") as? String
        user.cityID =  UserDefaults.standard.value( forKey: "cityid") as? String
        user.countryID =  UserDefaults.standard.value( forKey: "countryid") as? String
        user.maxCapacity =  UserDefaults.standard.value( forKey: "maxcapacity") as? String
        user.vehicleNumber =  UserDefaults.standard.value( forKey: "vehiclenumber") as? String
        
        user.vehicleName =  UserDefaults.standard.value( forKey: "vehiclename") as? String
        user.firstName =  UserDefaults.standard.value( forKey: "fname") as? String
        user.lastName =  UserDefaults.standard.value( forKey: "lname") as? String
        user.phone1 =  UserDefaults.standard.value( forKey: "phone1") as? String
        user.categoryName =  UserDefaults.standard.value( forKey: "categoryname") as? String
        
        user.companyName =  UserDefaults.standard.value( forKey: "companyname") as? String
        user.companyID =  UserDefaults.standard.value( forKey: "companyid") as? String
        user.permitID =  UserDefaults.standard.value( forKey: "permitid") as? String
        user.driverID =  UserDefaults.standard.value( forKey: "driverid") as? String
        user.dob =  UserDefaults.standard.value( forKey: "dob") as? String
        
        user.cabID =  UserDefaults.standard.value( forKey: "cabid") as? String
        user.boatHeight =  UserDefaults.standard.value( forKey: "boatheight") as? String
        user.image =  UserDefaults.standard.value( forKey: "image") as? String
        user.licenceID =  UserDefaults.standard.value( forKey: "licenceid") as? String
        user.loginDate =  UserDefaults.standard.value( forKey: "logindate") as? String
        
        user.rating =  UserDefaults.standard.value( forKey: "rating") as? String
        user.opLastName =  UserDefaults.standard.value( forKey: "oplastname") as? String
        user.opFirstName =  UserDefaults.standard.value( forKey: "opfirstname") as? String
        user.endPointErn =  UserDefaults.standard.value( forKey: "endpointern") as? String
        user.tagline =  UserDefaults.standard.value( forKey: "tagline")  as? String
        user.isMarine =  UserDefaults.standard.value( forKey: "ismarine") as? String
        user.dateCreated =  UserDefaults.standard.value( forKey: "datecreated") as? String
        return user.accessToken != nil ? user : nil
    }
    
    class func deleteUser(){

        AppController.sharedInstance.stopContinuousUpdation()
        UserDefaults.standard.set(nil, forKey: keyEmail)
        UserDefaults.standard.set(nil, forKey: keyPIN)
        UserDefaults.standard.set(nil, forKey: keyPassword)
        UserDefaults.standard.set(nil, forKey: keyIsAutologin)
        UserDefaults.standard.set(nil, forKey: keySessionToken)
        UserDefaults.standard.set(nil, forKey: keyAPI_KEY)
        UserDefaults.standard.removeObject(forKey: keyAPI_KEY)

        UserDefaults.standard.synchronize()
    }
    
    class func isStaySignedInChecked()->Bool?{
        if let _ = UserDefaults.standard.value(forKey: keyIsAutologin){
            return UserDefaults.standard.bool(forKey: keyIsAutologin)
        }
        return nil
    }
    
    class func getLoginDetails()->(uName:String?, pwd:String?){
        if let uname = UserDefaults.standard.value(forKey: keyEmail) as? String, let pwd = UserDefaults.standard.value(forKey: keyPassword) as? String{
            return(uname, pwd)
        }
        return(nil, nil)
    }
}

