//
//  ActionSheetHelper.swift
//  Bekon
//  Created by ZCO Engineering Dept. on 22/06/17.
//  Copyright © 2017 MOPL. All rights reserved.
//

import UIKit

enum OptionType {
    
    case cancel
    case destructive
    case `default`
}

enum ActionType {
    
    case alert
    case action
}

protocol ActionHelperDelegate {
    
    func messengerOptionTapped(_ option:ActionOption, inMessenger messenger:ActionSheetHelper) -> Void
    
}

class ActionOption {
    
    var messageOptionType:OptionType = OptionType.default
    var optionText:String = ""
    var index:Int = 0
    
    init(optionText opText:String)
    {
        self.messageOptionType = .default
        self.optionText = opText
    }
    
    init(optionText opText:String, optionType:OptionType)
    {
        self.messageOptionType = optionType
        self.optionText = opText
    }
}

class AlertAction: UIAlertAction {
    
    var option:ActionOption!
}

class ActionSheetHelper: NSObject {
    
    var delegate:ActionHelperDelegate?
    weak var controllerRef:UIViewController?
    var actionController:UIAlertController?
    
    var tag:Int = 0
    var index:Int = 0
    
    func showMessage(_ message:String, withTitle title:String, messageOptions options:[ActionOption], messageType msgType:ActionType) {
        
        self.index = 0
        
        var alertStyle = UIAlertControllerStyle.alert
        
        switch msgType {
            
        case .alert:
            alertStyle = .alert
            
        case .action:
            alertStyle = .actionSheet
            
        }
        
        var alertTitle: String? = title
        
        if (title == " ") {
            
            alertTitle = nil
        }
        
        var alertMessage: String? = message
        
        if (message == " ") {
            
            alertMessage = nil
        }
        
        self.actionController = UIAlertController(title:alertTitle, message:alertMessage, preferredStyle: alertStyle)
        
        for option in options
        {
            self.addOption(option, controller: self.actionController!)
        }
        
        controllerRef?.present(self.actionController!, animated: true, completion: nil)
    }
    
    fileprivate func addOption(_ option:ActionOption, controller ctlr:UIAlertController)
    {
        var actionStyle = UIAlertActionStyle.default
        
        switch option.messageOptionType {
            
        case .cancel:
            actionStyle = .cancel
            
        case .destructive:
            actionStyle = .destructive
            
        case .default:
            actionStyle = .default
            
        }
        
        let action = AlertAction(title: option.optionText, style:actionStyle) { (alertAction:UIAlertAction) in
            
            if let alAction = alertAction as? AlertAction
            {
                if let ref:ActionSheetHelper = self
                {
                    ref.delegate?.messengerOptionTapped(alAction.option, inMessenger:ref)
                }
            }
        }
        
        option.index = self.index
        self.index = self.index + 1
        action.option = option
        
        ctlr.addAction(action)
    }
    
    func dismiss() {
        
        self.actionController?.dismiss(animated: true, completion: nil)
        self.actionController = nil
    }
    
    deinit
    {
        print("Messenger deallocated")
    }
}

