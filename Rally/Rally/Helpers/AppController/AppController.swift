//
//  AppController.swift
//  Rally
//
//  Created by Jayakrishnan S on 27/03/18.
//  Copyright © 2018 Jayakrishnan.S. All rights reserved.
//

import UIKit

class AppController: NSObject {
    static let sharedInstance = AppController()
    var loggedInUser:RallyUserModel?{
        didSet{
            if loggedInUser != nil {
                ScreenManager.sharedInstance.loadHomeScreen()
            }
        }
    }
}
