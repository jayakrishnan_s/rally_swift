//
//  Validator.swift
//  Rally
//
//  Created by Jayakrishnan S on 21/11/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import Foundation
import UIKit

extension  UITextField {
    
    func isEmpty()-> Bool {
        if let txt = text{
            return txt.trimmingCharacters(in: .whitespacesAndNewlines) == ""
        }
        return false
    }
}

class Validator{
    
    class func validateTextFields(fields:[UITextField])->(Status: Bool, ErrorMessage:String){
         let nonEmptyFields = fields.filter {!$0.isEmpty()}
        if nonEmptyFields.count > 0{
            var message = "Field cannot be empty"
            if let placeHolder = nonEmptyFields[0].placeholder{
                message = placeHolder + message
            }
            return (false, message)
        }
        return (true, "")
    }
}
