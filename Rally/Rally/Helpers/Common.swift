//
//  Common.swift
//  Rally
//
//  Created by Jayakrishnan S on 10/10/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import Foundation
import UIKit


extension DispatchQueue {

    class func doAfter(_ delay: TimeInterval, execute closure: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: closure)
    }
}

class Common{
    
    
    /**
     Make the corners of the views in the specified array rounded.
     
     - parameter controlArray: Array of views.
     - parameter borderNeeded: Border needed or not.
     - parameter borderColor:  Border color if border is needed.
     - parameter roundness:    Corner radius.
     */
    static func makeControlCornersRounded(controlArray: [UIView], borderNeeded: Bool, borderColor: UIColor, roundness: CGFloat) {
        
        for i in 0 ..< controlArray.count {
            
            controlArray[i].layer.cornerRadius = roundness
            controlArray[i].layer.masksToBounds = true
            
            if (borderNeeded) {
                controlArray[i].layer.borderWidth = 1.0
                controlArray[i].layer.borderColor = borderColor.cgColor
            }
        }
    }

    /// To create view controller using storyboard name and viewcontroller identifier
    ///
    /// - parameter identifier:     identifier
    /// - parameter storyboardName: storyboardName
    ///
    /// - returns: Viewcontroller created using the parameters
    class func getViewControllerFor(identifier:String?, storyboardName:storyBoard) -> (UIViewController?) {
        
        if(nil == identifier ) {
            return nil;
        }
        
        let aStoryboard:UIStoryboard? = UIStoryboard(name: storyboardName.rawValue, bundle: Bundle.main)
        let aViewController:UIViewController? = aStoryboard?.instantiateViewController(withIdentifier: identifier!)
        return aViewController!;
    }
    
    /**
     Show simple alert with the input message on the specified view controller.
     
     - parameter message:    Message to be displayed on the alert window.
     - parameter controller: View controller on which the alert to be displayed.
     */
    static func showSimpleAlert(message: String, controller: UIViewController) {
        
        let alertController = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            
            alertController.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(okAction)
        DispatchQueue.main.async {
            controller.present(alertController, animated: true, completion: nil)
        }
    }
    
    static func showNotImplementedMessage(controller: UIViewController) {
        
        Common.showSimpleAlert(message: "Not Implemented.", controller: controller)
    }
    
    static func showNoInternetConnectivityMessage() {
        
        Common.showSimpleAlert(message: "No internet connection available.", controller: ScreenManager.sharedInstance.getTopViewController())
    }
    
    static func showServerConnectivityMessage() {
        
    }
    
    static func showWeakInternetConnectivityMessage() {
        
        Common.showSimpleAlert(message: "Internet connectivity is not available.", controller: ScreenManager.sharedInstance.getTopViewController())
    }

}
