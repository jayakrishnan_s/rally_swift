//
//  LocationViewController.swift
//  Rally
//
//  Created by Jayakrishnan S on 07/02/18.
//  Copyright © 2018 Jayakrishnan.S. All rights reserved.
//

import UIKit
import GooglePlacePicker

class LocationViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        getPlacePickerView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func getPlacePickerView() {
        
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
//        placePicker.delegate = self
//        view.addSubview(placePicker.view)
//        navigationController?.pushViewController(placePicker, animated: false)
//        present(placePicker, animated: false, completion: nil)
    }

}

extension LocationViewController : GMSPlacePickerViewControllerDelegate {
    // GMSPlacePickerViewControllerDelegate and implement this code.
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        
        /*self.lblName.text = place.name
         self.lblAddress.text = place.formattedAddress?.components(separatedBy: ", ")
         .joined(separator: "\n")
         self.lblLatitude.text = String(place.coordinate.latitude)
         self.lblLongitude.text = String(place.coordinate.longitude)*/
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        
        viewController.dismiss(animated: true, completion: nil)
    }
}

