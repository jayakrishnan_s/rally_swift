//
//  DatePickerViewController.swift
//  Rally
//
//  Created by Jayakrishnan S on 01/04/18.
//  Copyright © 2018 Jayakrishnan.S. All rights reserved.
//

import UIKit

protocol DatePickerProtocol {
    func dateAndTimeSelected(date:Date, chosenHours:[Hour])
}

class DatePickerViewController: BaseViewController {

    var flexibleTimePicker: FlexibleTimePicker?
    var calendarView: CLWeeklyCalendarView?
    var delegate:DatePickerProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addDateTimeView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addDateTimeView(){
        calendarView = CLWeeklyCalendarView(frame: CGRect(x: 0, y: 60, width: view.frame.size.width, height: 100))
        view.addSubview(calendarView!)
        calendarView!.selectedDate = Date()
        let container = CGRect(x: 0, y: calendarView!.frame.origin.y + 100, width: view.frame.size.width, height: self.view.frame.height - 160)
        flexibleTimePicker = FlexibleTimePicker(frame: container)
        flexibleTimePicker?.delegate = self
        self.view.addSubview(flexibleTimePicker!)
        self.setCustomProperties()
    }
    
    private func setCustomProperties() {
        flexibleTimePicker?.fromCurrentHour = false
        flexibleTimePicker?.allowsMultipleSelection = true
        flexibleTimePicker?.startHour = 0
        flexibleTimePicker?.endHour = 24
        flexibleTimePicker?.removeCellBorders = false
        flexibleTimePicker?.minuteFrequency = 15
        flexibleTimePicker?.cellBorderThickness = 1
        flexibleTimePicker?.cellBorderColor = UIColor.red
        flexibleTimePicker?.cellOnlyBottomBorder = true
        flexibleTimePicker?.cellHeight = 50
        flexibleTimePicker?.cellCountPerRow = 5
        flexibleTimePicker?.refreshUI()
    }

}

extension DatePickerViewController:FlexibleTimePickedDelegate{
    func timePicked(chosenHours:[Hour]){
        guard chosenHours.count == 2 else {
            print("Count not reached")
            return
        }
        delegate?.dateAndTimeSelected(date: calendarView!.selectedDate, chosenHours: chosenHours)
        navigationController?.popViewController(animated: false)
    }
}
