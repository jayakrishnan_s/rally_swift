//
//  AddEventController.swift
//  Rally
//
//  Created by Jayakrishnan S on 22/12/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import UIKit
import GooglePlacePicker
import Contacts

class AddEventController: BaseViewController, AECellDelegate {

    var dataSource:AddEventDataSource?
    @IBOutlet weak var tableView:UITableView!
    var items:[SwiftMultiSelectItem] = [SwiftMultiSelectItem]()
    var initialValues:[SwiftMultiSelectItem] = [SwiftMultiSelectItem]()
    var selectedItems:[SwiftMultiSelectItem] = [SwiftMultiSelectItem]()

    var rallyModel:RallyModel?
    let locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = AddEventDataSource()
        dataSource!.cellDelegate = self
        tableView.dataSource = dataSource!
        tableView.reloadData()
        if nil == rallyModel{
            rallyModel = RallyModel()
        }else if let locName = rallyModel?.locationName{

            dataSource?.setText(text:locName, .Add_Location , image: nil)
        }
        SwiftMultiSelect.dataSource     = self
        SwiftMultiSelect.delegate       = self
        getLocationPermission()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction override func backORCloseAction(_ id: Any) {
        super.backORCloseAction(id)
    }
    
    @IBAction func addEventAction(_ id: Any) {
        rallyModel?.createdBy = AppController.sharedInstance.loggedInUser
        rallyModel?.addedTime = Date()
        rallyModel?.updateToCoreData()
        super.backORCloseAction(id)
    }

    private func getLocationPermission(){
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            break
        default:
            locationManager.requestWhenInUseAuthorization()
        }
    }
    func getPlacePickerView() {
        
        ShowLoader(withText: "Loading Map..")
        GMSPlacesClient.shared().currentPlace(callback: { (placeList, error) in
            self.HideLoader()
            guard nil == error else{
                return
            }
            if let list = placeList?.likelihoods{
                let place = list[0]
                let viewPport = GMSCoordinateBounds(coordinate: place.place.coordinate, coordinate: list[1].place.coordinate)
                let config = GMSPlacePickerConfig(viewport: nil)
                let placePicker = GMSPlacePickerViewController(config: config)
                placePicker.delegate = self
                self.present(placePicker, animated: true, completion: nil)
            }
        })
        
    }

}

extension AddEventController{
    
    func buttonAction(_ type: AEButtonType) {
        switch type {
        case .Add_Contacts:
            SwiftMultiSelect.initialSelected = initialValues
            SwiftMultiSelect.Show(to: self)
        case .Add_Location:
            getPlacePickerView()
        case .Add_Image:
            launchCameraView()
        }
    }
    
    func textChanged(_ fieldType: AEFieldType, text: String) {
        // MARK: TODO
        switch fieldType {
        case .AEName:
            rallyModel!.eventTitle = text
        case .AEDescription:
            rallyModel!.eventDescription = text
        case .AEDuration:
            if let hourPicker = Common.getViewControllerFor(identifier: "DatePickerViewController", storyboardName: .main) as? DatePickerViewController{
                hourPicker.delegate = self
//                hourPicker.flexibleTimePicker.
                navigationController?.pushViewController(hourPicker, animated: false)
            }
        }
    }
}

extension AddEventController : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        switch indexPath.row {
        case 0:
            return 60
        case 1:
            return 130
        case 2:
            return 70
        case 3:
            return 60
        case 4:
            return 70
        case 5:
            return 70

        default:
            return 30
        }
    }
}

// MARK: SwiftMultiSelectDelegates

extension AddEventController: SwiftMultiSelectDelegate, SwiftMultiSelectDataSource{
    
    func userDidSearch(searchString: String) {
        if searchString == ""{
            selectedItems = items
        }else{
            selectedItems = items.filter({$0.title.lowercased().contains(searchString.lowercased()) || ($0.description != nil && $0.description!.lowercased().contains(searchString.lowercased())) })
        }
    }
    
    func numberOfItemsInSwiftMultiSelect() -> Int {
        return selectedItems.count
    }
    
    func swiftMultiSelect(didUnselectItem item: SwiftMultiSelectItem) {
        print("row: \(item.title) has been deselected!")
    }
    
    func swiftMultiSelect(didSelectItem item: SwiftMultiSelectItem) {
        print("item: \(item.title) has been selected!")
    }
    
    func didCloseSwiftMultiSelect() {
//        badge.isHidden = true
//        badge.text = "" hfdjhf djkfkhfn
    }
    
    func swiftMultiSelect(itemAtRow row: Int) -> SwiftMultiSelectItem {
        return selectedItems[row]
    }
    
    func swiftMultiSelect(didSelectItems items: [SwiftMultiSelectItem]) {
        if nil == rallyModel?.members{
            rallyModel?.members = [RallyUser]()
        }
        initialValues   = items
        print("you have selected: \(items.count) items!")
        for (index,element)  in items.enumerated(){
            print(element.string)
            if let contact = element.userInfo as? CNContact{
                updateUserToCoredata(contact,index: Int32(index))
            }
        }
        dataSource?.setText(text: "\(items.count) Friends", .Add_Contacts, image: nil)
        tableView?.reloadData()
    }
    
    func updateUserToCoredata(_ contact:CNContact, index:Int32 = 0){
        let id = contact.identifier

        guard let savedModel = RallyUser.fetchObject(withAttributeName: "contactIdentifier", attributeValue: id) else {
            if let context = CoreDataManager.sharedInstance.managedObjectContext{
                let managedObjectModel = RallyUser(context: context)
                managedObjectModel.firstName = contact.givenName
                managedObjectModel.lastName = contact.familyName
                managedObjectModel.contactIdentifier = contact.identifier
                managedObjectModel.contactIndex = index
                let value = contact.emailAddresses
                managedObjectModel.email =  String(describing: value.first?.value)
                rallyModel?.members?.append(managedObjectModel)
                context.saveMe()
            }
            return
        }
        savedModel.firstName = contact.givenName
        savedModel.lastName = contact.familyName
        savedModel.contactIndex = index
        let value = contact.emailAddresses
        savedModel.email = String(describing: value.first?.value)
        savedModel.contactIdentifier = contact.identifier
        rallyModel?.members?.append(savedModel)
        if let context = CoreDataManager.sharedInstance.managedObjectContext{
            context.saveMe()
        }
    }

}

extension AddEventController : GMSPlacePickerViewControllerDelegate {
    // GMSPlacePickerViewControllerDelegate and implement this code.
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        
        viewController.dismiss(animated: true, completion: nil)
        
        rallyModel?.locationName = place.name
        rallyModel?.location = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        dataSource?.setText(text: place.name, .Add_Location , image: nil)
        tableView?.reloadData()
        /*self.lblName.text = place.name
        self.lblAddress.text = place.formattedAddress?.components(separatedBy: ", ")
            .joined(separator: "\n")
        self.lblLatitude.text = String(place.coordinate.latitude)
        self.lblLongitude.text = String(place.coordinate.longitude)*/
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        
        viewController.dismiss(animated: true, completion: nil)
    }
}

extension AddEventController:  ImagePickerCallBack{
    func launchCameraView() {
        
        let settings = ImagePickerSettings()
        settings.isEditable = true
        settings.cameraMode = .photo
        settings.cameraType = .rear
        
        ProfileImageHelper().showImagePickerWithSourceOptions(FromController: self
            , withSettings: settings, delegate: self)
    }
    
    func imagePickerDidFinishPickingImage(image: UIImage) {
        dataSource?.setText(text: "", .Add_Image , image: image)
        rallyModel?.eventProfilePic = image
        tableView?.reloadData()
    }
    
    func imagePickerDidCancel() {
        print("image picker canceled.")
    }
}

extension AddEventController: DatePickerProtocol{
    func dateAndTimeSelected(date:Date, chosenHours:[Hour]){
        rallyModel?.eventDate = date
        rallyModel?.hours = "\(chosenHours[0].hourString) - \(chosenHours[1].hourString)"
        dataSource?.setText(text: (rallyModel?.hours)!, nil, fieldType: .AEDuration , image: nil)
        tableView?.reloadData()
    }
}
