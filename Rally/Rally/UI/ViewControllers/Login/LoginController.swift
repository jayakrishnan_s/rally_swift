//
//  LoginController.swift
//  Rally
//
//  Created by Jayakrishnan S on 23/10/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit

class LoginController: BaseViewController, GIDSignInUIDelegate {

    @IBOutlet weak var txtUserName:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var btnGoogleSignin:GIDSignInButton!
    @IBOutlet weak var btnSignin:UIButton!
    @IBOutlet weak var btnFacebookSignin:FBSDKLoginButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        btnFacebookSignin.readPermissions = ["public_profile", "email"];
        btnFacebookSignin.delegate = self
        initialiseUI()
        if let _ = FBSDKAccessToken.current(){
//            btnFacebookSignin.isEnabled = false
            fetchUserData()
        }else if GIDSignIn.sharedInstance().hasAuthInKeychain(){
            print("Google signed in")
        }
    }
    
    private func initialiseUI(){
        Common.makeControlCornersRounded(controlArray: [btnSignin], borderNeeded: false, borderColor: .clear, roundness: btnSignin.frame.size.height/2)
    }
    
    
    // Mark: Button Actions
    
    @IBAction func actionLogin(_ sender:UIButton){
        Common.showSimpleAlert(message: "Normal login yet to be implemented.Use Facebook or Google signin", controller: self)
//        ScreenManager.sharedInstance.loadHomeScreen()
    }
    
    @IBAction func actionSignup(_ sender:UIButton){
        Common.showNotImplementedMessage(controller: self)
    }

    @IBAction func actionForgotPassword(_ sender:UIButton){
        Common.showNotImplementedMessage(controller: self)
    }


}

extension LoginController{
    func signIn(signIn: GIDSignIn!,
                presentViewController viewController: UIViewController!) {
        ShowLoader(withText: "Signing in with google..")
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func signIn(signIn: GIDSignIn!,
                dismissViewController viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
        self.HideLoader()
    }
}

extension LoginController : GIDSignInDelegate{
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            let rallyUser = RallyUserModel(withGoogleData: user)
            AppController.sharedInstance.loggedInUser = rallyUser
            // ...
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
}

extension LoginController:FBSDKLoginButtonDelegate{
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        guard nil == error else {
            print("Facebook login failed. Error -> \(error!.localizedDescription)")
            return
        }
        loginButton.isEnabled = false
        fetchUserData()
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        loginButton.isEnabled = true
    }
    
    /**
     Fetch facebook user details
     
     - parameter handler: completion handler
     */
    func fetchUserData() {
        
        let params = ["fields": "email,first_name,last_name,picture"]//,user_birthday,user_location
        
        let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: params
        )
        
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            guard let _ = result else{
                print(error?.localizedDescription)
                return
            }
            
            print(result)
            if let dict = result as? NSDictionary{
                let usr = self.parseFbResult(resultData: dict)
                AppController.sharedInstance.loggedInUser = usr
            }
        })
    }

    private func parseFbResult(resultData result:NSDictionary)->RallyUserModel?{
        
        return RallyUserModel(withFacebookData: result)
    }

}

