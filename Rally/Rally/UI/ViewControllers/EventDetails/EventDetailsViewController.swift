//
//  EventDetailsViewController.swift
//  Rally
//
//  Created by Jayakrishnan S on 30/03/18.
//  Copyright © 2018 Jayakrishnan.S. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI
import GooglePlacePicker

class EventDetailsViewController: BaseViewController {

    enum EDCellIdentifier:String {
        case Description = "EDDescription"
        case Members = "Members"
        case DateTime = "DateTime"
        case Location = "Location"
        case Profile = "Profile"
    }
    
    @IBOutlet weak var navigationBar:UINavigationBar!
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var eventImage:UIImageView!
    var modalEvent:RallyModel?{
        didSet{
            guard let _ = modalEvent else{
                return
            }
//            var managedModel = Event.fetchObject(withAttributeName: "eventId", attributeValue: modalEvent?.eventId!)
//            managedModel = modalEvent
            /*managedModel?.addedDate = modalEvent?.addedDate
            managedModel?.eventId = modalEvent?.eventId
            managedModel?.eventProfilePic = modalEvent?.eventProfilePic
            managedModel?.eventDate = modalEvent?.eventDate
            managedModel?.eventDescription = modalEvent?.eventDescription
            managedModel?.hours = modalEvent?.hours
            if let lat = modalEvent?.latitude{
                managedModel?.latitude = lat
            }
            if let lat = modalEvent?.longitude{
                managedModel?.longitude = lat
            }
            managedModel?.locationName = modalEvent?.locationName
            managedModel?.startTime = modalEvent?.startTime
            managedModel?.endTime = modalEvent?.endTime
            managedModel?.title = modalEvent?.title
            managedModel?.createdBy = modalEvent?.createdBy*/
        }
    }
    var currentMembers:[RallyUser] = [RallyUser]()
    var contacts:[SwiftMultiSelectItem]?{
        didSet{
            if nil != contacts{
                initialValues = contacts!
                selectedItems = contacts!
            }
        }
    }
    var items:[SwiftMultiSelectItem] = [SwiftMultiSelectItem]()
    var initialValues:[SwiftMultiSelectItem] = [SwiftMultiSelectItem]()
    var selectedItems:[SwiftMultiSelectItem] = [SwiftMultiSelectItem]()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar?.topItem?.title = modalEvent?.eventTitle
        getContactDetails()
        SwiftMultiSelect.dataSource     = self
        SwiftMultiSelect.delegate       = self
        if let data = modalEvent?.eventProfilePic{
            eventImage.image = data
        }
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 64.0;
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        modalEvent?.updateToCoreData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func getContactDetails(){
        guard let eventModal = modalEvent, let members = eventModal.members else {
            return
        }
        let contactIds = members.flatMap { (user) -> String? in
            if let userData = user as? RallyUser, let id = userData.contactIdentifier {
                return id
            }else{
                return ""
            }
        }
        
        let predicate = CNContact.predicateForContacts(withIdentifiers: contactIds)
        ContactsLibrary.getContacts(predicate:  predicate) { (success, items) in
            let filteredItems = items?.filter({ (selected) -> Bool in
                return contactIds.contains(selected.contactId!)
            })
            self.contacts = filteredItems
            DispatchQueue.main.async {
                self.tableView?.reloadData()
            }
        }
    }
    
    @IBAction override func backORCloseAction(_ id: Any) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func chatAction(_ id: Any) {
        Common.showNotImplementedMessage(controller: self)
    }

}

extension EventDetailsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:EventDetailsBaseCell?
        switch indexPath.row {
        case 0:
            if let vCell = tableView.dequeueReusableCell(withIdentifier: EDCellIdentifier.Description.rawValue) as? EDDescriptionCell{
                vCell.lblDescription.text = modalEvent?.eventDescription
                cell = vCell
            }
            
        case 1:
            if let vCell = tableView.dequeueReusableCell(withIdentifier: EDCellIdentifier.Members.rawValue) as? EDEventMemebersCell{
                if let _ = contacts{
                    vCell.selectedItems = contacts!
                }
                vCell.delegate = self
                cell = vCell
            }
        case 2:
            if let vCell = tableView.dequeueReusableCell(withIdentifier: EDCellIdentifier.DateTime.rawValue) as? EDDurationCell{
                vCell.lblDate.text = modalEvent?.eventDate?.toWeekDayAndDateString()
                if let start = modalEvent?.hours{
                    vCell.lblTimeFrame.text = start
                }
                cell = vCell
            }
        case 3:
            if let vCell = tableView.dequeueReusableCell(withIdentifier: EDCellIdentifier.Location.rawValue) as? EDLocationCell{
                vCell.delegate = self
                vCell.lblLocationName.text = modalEvent?.locationName
                cell = vCell
            }
        case 4:
            if let vCell = tableView.dequeueReusableCell(withIdentifier: EDCellIdentifier.Profile.rawValue) as? EDProfileCell{
                vCell.lblProfileName.text = modalEvent?.createdBy?.firstName
                if let url = modalEvent?.createdBy?.profilePic{
                    vCell.profilePic.setUrl(imageUrl: url)
                }
                cell = vCell
            }
        default:
            return UITableViewCell()
        }
        
        guard let cellInstance = cell else{
            return UITableViewCell()
        }
        return cellInstance
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
            return UITableViewAutomaticDimension
    }

}

extension EventDetailsViewController: AECellDelegate{
    func buttonAction(_ type: AEButtonType) {
        if type == .Add_Contacts{
            SwiftMultiSelect.initialSelected = initialValues
            SwiftMultiSelect.Show(to: self)
        }else if type == .Add_Location{
            ShowLoader(withText: "Loading Map..")
            GMSPlacesClient.shared().currentPlace(callback: { (placeList, error) in
                self.HideLoader()
                guard nil == error else{
                    return
                }
                if let list = placeList?.likelihoods{
                    let place = list[0]
                    let googleMapUrlString = "http://maps.google.com/?saddr=\(place.place.coordinate.latitude),\(place.place.coordinate.longitude)&daddr=\(self.modalEvent?.location?.coordinate.latitude),\(self.modalEvent?.location?.coordinate.longitude)"
                    if let url = URL(string:googleMapUrlString){
                        UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                            print("succeeded")
                        })
                    }
                }
            })


        }
    }
}

// MARK: SwiftMultiSelectDelegates

extension EventDetailsViewController: SwiftMultiSelectDelegate, SwiftMultiSelectDataSource{
    
    func userDidSearch(searchString: String) {
        if searchString == ""{
            selectedItems = items
        }else{
            selectedItems = items.filter({$0.title.lowercased().contains(searchString.lowercased()) || ($0.description != nil && $0.description!.lowercased().contains(searchString.lowercased())) })
        }
    }
    
    func numberOfItemsInSwiftMultiSelect() -> Int {
        return selectedItems.count
    }
    
    func swiftMultiSelect(didUnselectItem item: SwiftMultiSelectItem) {
        print("row: \(item.title) has been deselected!")
    }
    
    func swiftMultiSelect(didSelectItem item: SwiftMultiSelectItem) {
        print("item: \(item.title) has been selected!")
    }
    
    func didCloseSwiftMultiSelect() {
        //        badge.isHidden = true
        //        badge.text = "" hfdjhf djkfkhfn
    }
    
    func swiftMultiSelect(itemAtRow row: Int) -> SwiftMultiSelectItem {
        return selectedItems[row]
    }
    
    func swiftMultiSelect(didSelectItems items: [SwiftMultiSelectItem]) {
//        if nil == modalEvent?.members{
//            modalEvent?.members = [RallyUser]()
//        }
        contacts = items
        tableView?.reloadData()
        initialValues   = items
        
        let users = RallyUser.fetchObjectList()
        if let thisEvent = Event.fetchObject(withAttributeName: "eventId", attributeValue: modalEvent!.eventId){
            users?.forEach({ (user) in
                user.removeFromEvents(thisEvent)
            })
        }

        print("you have selected: \(items.count) items!")
        currentMembers.removeAll()
        for item  in items{
            print(item.string)
            if let contact = item.userInfo as? CNContact{
                updateUserToCoredata(contact)
            }
        }
        modalEvent?.members = currentMembers
    }
    
    func updateUserToCoredata(_ contact:CNContact){
        let id = contact.identifier
        
        guard let savedModel = RallyUser.fetchObject(withAttributeName: "contactIdentifier", attributeValue: id) else {
            if let context = CoreDataManager.sharedInstance.managedObjectContext{
                let managedObjectModel = RallyUser(context: context)
                managedObjectModel.firstName = contact.givenName
                
                managedObjectModel.lastName = contact.familyName
                managedObjectModel.contactIdentifier = contact.identifier
                let value = contact.emailAddresses
                managedObjectModel.email =  String(describing: value.first?.value)
                currentMembers.append(managedObjectModel)
                context.saveMe()
            }
            return
        }
        savedModel.firstName = contact.givenName
        savedModel.lastName = contact.familyName
        let value = contact.emailAddresses
        savedModel.email = String(describing: value.first?.value)
        savedModel.contactIdentifier = contact.identifier
        currentMembers.append(savedModel)
        if let context = CoreDataManager.sharedInstance.managedObjectContext{
            context.saveMe()
        }
    }
    
}
