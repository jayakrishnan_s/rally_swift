//
//  SplashController.swift
//  Rally
//
//  Created by Jayakrishnan S on 08/11/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import UIKit

class SplashController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.doAfter(2) {
            ScreenManager.sharedInstance.loadLoginScreen()
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
