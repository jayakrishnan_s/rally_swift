//
//  HomeRallyViewController.swift
//  Rally
//
//  Created by Jayakrishnan S on 22/11/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit

class HomeRallyViewController: BaseViewController {

    var eventList:[RallyModel] = [RallyModel]()
    
    @IBOutlet weak var tblEventList:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblEventList.rowHeight = UITableViewAutomaticDimension;
        self.tblEventList.estimatedRowHeight = 360;
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let eventList = Event.fetchObjectList()
        self.eventList.removeAll()
        eventList?.forEach({ (event) in
            let rally = RallyModel(with: event)
            self.eventList.append(rally)
        })
        tblEventList.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func addEvent(_ sender: UIBarButtonItem) {
        ScreenManager.sharedInstance.loadAddEventController()
    }
    
    @IBAction func logout(_ sender: UIBarButtonItem) {
        if let user = AppController.sharedInstance.loggedInUser{
            if let _ = user.googleUserId{
                GIDSignIn.sharedInstance().signOut()
            }
            
            if let _ = user.facebookUserId{
                FBSDKLoginManager().logOut()
            }
//            if let managedObjectMOdel = RallyUser.fetchObject(withAttributeName: "email", attributeValue: user.email){
//                RallyUser.remove(object: managedObjectMOdel)
//            }
        }
        Event.removeAllObjects()
        AppController.sharedInstance.loggedInUser = nil
        ScreenManager.sharedInstance.loadLoginScreen()
    }

}

extension HomeRallyViewController: UITabBarDelegate, UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "rallyCell") as? RallyHomeTableViewCell{
            let event = eventList[indexPath.row]
            cell.lblTitle.text = event.eventTitle
            cell.lblDescription.text = event.eventDescription
            cell.lblLocation.text = event.locationName
            cell.lblUser_Name.text = event.createdBy!.firstName! + " " + event.createdBy!.lastName!
            if let addedOn = event.eventDate{
                cell.lblUpdatedTime.text = addedOn.timeAgo(numericDates: true)
            }
            if let url = event.createdBy!.profilePic {
                cell.avatarImageView.setUrl(imageUrl: url)
            }
            cell.lblRallyDuration.text = event.hours
            if let image = event.eventProfilePic {
                cell.rallyImageView.image = image
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableViewAutomaticDimension
//        return 310.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if let detailCtrlr = Common.getViewControllerFor(identifier: "EventDetailsViewController", storyboardName: .main) as? EventDetailsViewController{
            let rallyObject = eventList[indexPath.row]

            detailCtrlr.modalEvent = rallyObject
            navigationController?.navigationItem.title = rallyObject.eventTitle
            navigationController?.pushViewController(detailCtrlr, animated: true)
        }
    }
    
}
