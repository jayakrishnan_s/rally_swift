//
//  HomeTabController.swift
//  Rally
//
//  Created by Jayakrishnan S on 23/10/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import UIKit

class HomeTabController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedIndex = 0
        self.tabBar.unselectedItemTintColor = .white
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
}
