//
//  EDLocationCell.swift
//  Rally
//
//  Created by Jayakrishnan S on 02/04/18.
//  Copyright © 2018 Jayakrishnan.S. All rights reserved.
//

import UIKit

class EDLocationCell: EventDetailsBaseCell {

    @IBOutlet weak var lblLocationName:UILabel!

    @IBAction func showONMapAction(_ id:UIButton){
        delegate?.buttonAction(.Add_Location)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
