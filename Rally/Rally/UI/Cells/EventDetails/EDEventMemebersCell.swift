//
//  EDEventMemebersCell.swift
//  Rally
//
//  Created by Jayakrishnan S on 02/04/18.
//  Copyright © 2018 Jayakrishnan.S. All rights reserved.
//

import UIKit
import BubblePictures
import Contacts

class EDEventMemebersCell: EventDetailsBaseCell {

    @IBOutlet weak var collectionView:UICollectionView!
    private var bubblePictures: BubblePictures!
    public var bubbleDelegate: BPDelegate?
    var selectedItems : [SwiftMultiSelectItem] = [SwiftMultiSelectItem](){
        didSet{
            if selectedItems.count > 0{
                collectionView.reloadData()
            }
        }
    }
    
    @IBAction func addAction(_ id:UIButton){
        delegate?.buttonAction(.Add_Contacts)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}

extension EDEventMemebersCell:UICollectionViewDataSource,UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.selectedItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! EDMembersCollectionViewCell
        
        //Try to get item from delegate
        var item = self.selectedItems[indexPath.row]
        
        cell.lblTitle?.text        = item.title

        cell.imageView.isHidden   = true
        
        //Test if items it's CNContact
        if let contact = item.userInfo as? CNContact{
            DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {

                //Build contact image in background
                if(contact.imageDataAvailable && contact.imageData!.count > 0){
                    let img = UIImage(data: contact.imageData!)
                    DispatchQueue.main.async {
                        item.image = img
                        cell.imageView.image      = img
                        cell.lblInitials.isHidden      = true
                        cell.imageView.isHidden   = false
                    }
                }else{
                    DispatchQueue.main.async {
                        cell.lblInitials.text          = item.getInitials()
                        cell.lblInitials.isHidden      = false
                        cell.imageView.isHidden   = true
                    }
                }
                
            }
            
            //Item is custom type
        }else{
            if item.image == nil && item.imageURL == nil{
                cell.lblInitials.text          = item.getInitials()
                cell.lblInitials.isHidden      = false
                cell.imageView.isHidden   = true
            }else{
                if item.imageURL != ""{
                    cell.lblInitials.isHidden      = true
                    cell.imageView.isHidden   = false
                    cell.imageView.setImageFromURL(stringImageUrl: item.imageURL!)
                }else{
                    cell.imageView.image      = item.image
                    cell.lblInitials.isHidden      = true
                    cell.imageView.isHidden   = false
                }
            }
        }
        
        //Set item color
        if item.color != nil{
            cell.lblInitials.backgroundColor = item.color!
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        
        return CGSize(width: CGFloat(Config.selectorStyle.selectionHeight),height: CGFloat(Config.selectorStyle.selectionHeight))
    }
    
    @objc func handleTap(sender:UIButton){
        
        //Find current item
        let item = selectedItems.filter { (itm) -> Bool in
            itm.row == sender.tag
            }.first
        
        
        if self.selectedItems.count <= 0 {
            //Comunicate deselection to delegate
//            toggleSelectionScrollView(show: false) jkjk
        }
        
    }
    

    
    
}

