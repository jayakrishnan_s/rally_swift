//
//  EDDurationCell.swift
//  Rally
//
//  Created by Jayakrishnan S on 02/04/18.
//  Copyright © 2018 Jayakrishnan.S. All rights reserved.
//

import UIKit

class EDDurationCell: EventDetailsBaseCell {

    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lblTimeFrame:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
