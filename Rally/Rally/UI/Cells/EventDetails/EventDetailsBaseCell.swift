//
//  EventDetailsBaseCell.swift
//  Rally
//
//  Created by Jayakrishnan S on 02/04/18.
//  Copyright © 2018 Jayakrishnan.S. All rights reserved.
//

import UIKit

class EventDetailsBaseCell: UITableViewCell {

    var delegate:AECellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
