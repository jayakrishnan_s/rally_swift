//
//  EDMembersCollectionViewCell.swift
//  Rally
//
//  Created by Jayakrishnan S on 03/04/18.
//  Copyright © 2018 Jayakrishnan.S. All rights reserved.
//

import UIKit

class EDMembersCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblInitials:UILabel!
    @IBOutlet weak var imageView:UIImageView!
}
