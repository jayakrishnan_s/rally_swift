//
//  AddEventNameCell.swift
//  Rally
//
//  Created by Jayakrishnan S on 26/12/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import UIKit

class AddEventNameCell: AddEventBaseCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension AddEventNameCell:UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        let text = textField.text ?? ""
        delegate?.textChanged(.AEName, text: text)
    }
}
