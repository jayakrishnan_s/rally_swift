//
//  AddEventDurationCell.swift
//  Rally
//
//  Created by Jayakrishnan S on 26/12/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import UIKit

class AddEventDurationCell: AddEventBaseCell {

    @IBOutlet weak var txtHours:UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        delegate?.textChanged(AEFieldType.AEDuration, text: "")
    }

}

extension AddEventDurationCell:UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.textChanged(AEFieldType.AEDuration, text: "")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else {
            return
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard range.location <= 4 else{
            return false
        }
        
        if range.location == 2 && string != ""{
            if let _ = textField.text{
                textField.text!.append(":")
            }
        }else if range.location == 3 && string == ""{
            textField.text!.removeLast()
        }
        return true
    }
}
