//
//  AddEventButtonCell.swift
//  Rally
//
//  Created by Jayakrishnan S on 26/12/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import UIKit

class AddEventButtonCell: AddEventBaseCell {

    var buttontype:AEButtonType?{
        didSet{
            if let type = buttontype{
                switch type{
                case .Add_Contacts:
                    lblTitle?.text = "Friends"
                case .Add_Location:
                    lblTitle?.text = "Location"
                default:
                    lblTitle?.text = "Image"
                    eventImageView?.isHidden = false
                }
            }
        }
    }
    @IBOutlet weak var lblTitle:UILabel?
    @IBOutlet weak var eventImageView: UIImageView?

    func setTitle(_ text:String){
        lblTitle?.text = text
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func buttonAction(button: UIButton){
        guard let type = buttontype else{
            return
        }
        delegate?.buttonAction(type)
    }

}
