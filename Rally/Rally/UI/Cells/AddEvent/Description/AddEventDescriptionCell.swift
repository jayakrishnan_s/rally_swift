//
//  AddEventDescriptionCell.swift
//  Rally
//
//  Created by Jayakrishnan S on 26/12/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import UIKit

class AddEventDescriptionCell: AddEventBaseCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension AddEventDescriptionCell: UITextViewDelegate{
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        delegate?.textChanged(.AEDescription, text: textView.text)
    }
}
