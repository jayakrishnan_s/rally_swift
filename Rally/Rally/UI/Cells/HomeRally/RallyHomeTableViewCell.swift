//
//  RallyHomeTableViewCell.swift
//  Rally
//
//  Created by Jayakrishnan S on 25/03/18.
//  Copyright © 2018 Jayakrishnan.S. All rights reserved.
//

import UIKit

class RallyHomeTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImageView:UIImageView!
    @IBOutlet weak var rallyImageView:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblLocation:UILabel!
    @IBOutlet weak var lblRallyDuration:UILabel!
    @IBOutlet weak var lblUser_Name:UILabel!
    @IBOutlet weak var lblUpdatedTime:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
