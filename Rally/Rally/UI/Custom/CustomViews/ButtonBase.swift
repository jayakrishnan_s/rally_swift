//
//  FMButtonBase.swift
//  FuseMap
//
//  Created by Jayakrishnan S on 04/11/16.
//  Copyright © 2016 jk. All rights reserved.
//

import UIKit

class ButtonBase: UIButton{
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }

    func setImageEdgeInset(top:Double, left : Double, bottom: Double, right:Double){
        imageEdgeInsets = UIEdgeInsetsMake(CGFloat(top), CGFloat(left), CGFloat(bottom), CGFloat(right));
    }
    
    func setTitleInsets(top:Double, left : Double, bottom: Double, right:Double){
        titleEdgeInsets = UIEdgeInsetsMake(CGFloat(top), CGFloat(left), CGFloat(bottom), CGFloat(right));
    }
    
   }


class ViewCustom: UIView{
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
}

class LabelBase: UILabel{
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            if cornerRadius == -1{
                layer.cornerRadius = self.frame.size.width/2
                layer.masksToBounds = true
            }else{
                layer.cornerRadius = cornerRadius
                layer.masksToBounds = cornerRadius > 0
            }
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cornerRadius = frame.size.height/2
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard let key = keyPath else{
            return
        }
        
        if key == "frame" {
            print("frame changed")
            cornerRadius = frame.size.height/2
        }
    }
    
    override func didChangeValue(forKey key: String) {
        if key == "frame" {
            print("height changed")
        }
    }
}
