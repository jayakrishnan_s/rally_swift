//
//  CustomViewBase.swift
//  Bekon
//
//  Created by Jayakrishnan S on 06/07/17.
//  Copyright © 2017 MOPL. All rights reserved.
//

import UIKit

class CustomViewBase: UIView {
    
    func nibName() throws -> String {
        
        NSException(name: NSExceptionName(rawValue: "nibName not implemented"), reason: "nibName not implemented in class \(NSStringFromClass(type(of: self)))", userInfo: nil).raise()
        return ""
    }
    
    func nibObjectIndex() -> Int {
        return 0
    }
    
    func configureView() throws {
        
        NSException(name: NSExceptionName(rawValue: "configureView: not implemented"), reason: "configureView: not implemented in class \(NSStringFromClass(type(of: self)))", userInfo: nil).raise()
    }
    
    func Reload() throws {
        
        NSException(name: NSExceptionName(rawValue: "Reload: not implemented"), reason: "Reload: not implemented in class \(NSStringFromClass(type(of: self)))", userInfo: nil).raise()
    }
    
    
    // : Mark Life cycle
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        
        var view =  CustomViewBase()
        if (self.subviews.count == 0) {
            
            do {
                let name = try self.nibName()
                let nibObjects = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
                view = nibObjects?[0] as! CustomViewBase
                view.translatesAutoresizingMaskIntoConstraints = false
                do {
                    try view.configureView()
                }
                catch {
                    print("Configure View Not implemented")
                }
            }
            catch {
                print("NibName Nil")
            }
        } else {
            view = self;
        }
        
        return view;
    }
    
}
