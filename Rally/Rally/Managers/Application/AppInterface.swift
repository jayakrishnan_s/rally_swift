//
//  AppInterface.swift
//  Cabo Driver
//
//  Created by Jayakrishnan S on 09/05/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import Foundation

protocol AppInterface {
    
    func loadLogin()
    func loadSplash()
    func loadMain()
    func showUI(identifier:String, storyBoardName:String)
    func getCurrentLocation(completion: @escaping LocationUpdateBlock)
}
