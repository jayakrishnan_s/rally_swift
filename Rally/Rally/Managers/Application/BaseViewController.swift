//
//  BAseViewController.swift
//  Cabo Driver
//
//  Created by Jayakrishnan.S on 06/05/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import UIKit

enum PresentationStyle {
    
    case MODAL
    case TRANSPARENTMODAL
    case PUSH
    case ROOT
}

enum TransitionSubtype {
    case fromLeft
    case fromRight
    case fromTop
    case fromBottum
}

enum StatusBarStyle:Int {
    case light
    case dark
}

protocol navigationActions {
    
}

extension navigationActions{
    
    func backOrClose(_ animated:Bool? = true) {
        
    }
}

@objc class BaseViewController: UIViewController, navigationActions {


    fileprivate var bookingID:String?
    var viewControllerPresentationStyle: PresentationStyle = .MODAL
    var isTransparentBackground:Bool    = false
    var isNavigationBarSeperator:Bool   = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func backORCloseAction(_ id  : Any){
        backOrClose(true)
    }
    
    func backOrClose(_ animated: Bool?) {
        
        var isAnimated = true
        if let _ = animated{
            isAnimated = animated!
        }
        
        switch self.viewControllerPresentationStyle {
        case .PUSH:
            dismissDetail(animated:isAnimated)
            break
        case .MODAL, .TRANSPARENTMODAL:
            guard let navi = navigationController else{
                self.dismiss(animated: isAnimated, completion: nil)
                return
            }
            navi.dismiss(animated: isAnimated, completion: nil)
        case .ROOT:
            moveBackFromRootController()
            break
        }
    }
    
    func moveBackFromRootController() {
        // : TODO
    }
    
    func dismissDetail(animated:Bool) {
        
        self.navigationController?.popViewController(animated: animated)
    }

}

