//
//  ApplicationViewController.swift
//  Cabo Driver
//
//  Created by Jayakrishnan.S on 06/05/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import UIKit

enum StoryBoardID:String {
    case Login = "Logon"
    
}
class ApplicationViewController:UIViewController, AppInterface{

    var currentMainViewController:UIViewController?

    var currentLocation: [AnyHashable: Any?]?{
        willSet{
//            AppController.sharedInstance.currentLocation = newValue ?? nil
        }
    }
    var APIKey: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar()
        loadSplash()
        // Do any additional setup after loading the view.
    }
    
    func getCurrentLocation(completion: @escaping LocationUpdateBlock){
        
        LocationManager.shared().getCurrentLocation { (status, dictionary, error) in
            guard let _ = dictionary else{
                completion(status, dictionary, error)
                return
            }
            self.currentLocation = dictionary as! [String:Any]
            completion(status, dictionary, error)
            print(dictionary ?? "failed to get location")
        }        
    }

    override func viewDidAppear(_ animated: Bool) {
        
//        if let _ = Preferance.getSavedUser(){
//            return
//        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    private func setMain(vc:UIViewController){

    }
    
    func loadSplash(){
        ScreenManager.sharedInstance.loadSplash()
    }

    func loadLogin(){
//        showUI(type: .loginScreen, item: SideMenuItem())
    }
    
    func loadMain(){
        
    }
    
    func showUI(identifier: String, storyBoardName: String) {
        
    }
    
    private func setUpNavigationBar(){
//        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: .default)
//        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().barTintColor = ColorPalette.AppRedColor
//        UINavigationBar.appearance().isTranslucent = true
//        UINavigationBar.appearance().clipsToBounds = true
        UINavigationBar.appearance().backgroundColor = ColorPalette.AppRedColor
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
}

