//
//  ScreenManager.swift
//  Rally
//
//  Created by Jayakrishnan S on 21/11/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import Foundation
import UIKit
import GooglePlacePicker


typealias PreScreenNavigationBlock = (_ controller:BaseViewController) -> Void

var tabBarController : ESTabBarController?

class ScreenManager:NSObject{
    
    static let sharedInstance = ScreenManager()
    
    let kNavigationTitleImageSize   = CGRect(x: 0.0, y: 0.0, width: 126.0, height: 44.0)
    let kNavigationTitleImageName   = "navigationTitleLogo"
    
    //MARK:- Functions to load screens
    
    func loadSplash() {
        if let splash = Common.getViewControllerFor(identifier: "splash", storyboardName: .splash){
            if let wind = getWindow() {
                wind.rootViewController = splash
                wind.makeKeyAndVisible()
            }
        }
    }
    
    func loadRegistrationScreen() {
        
    }
    
    func loadUserAgreementScreen(parentController:UIViewController) {
        
    }
    
    func loadLoginScreen() {
        
        if let login = Common.getViewControllerFor(identifier: "login", storyboardName: .login) {
            if let wind = getWindow() {
                wind.rootViewController = login
                wind.makeKeyAndVisible()
            }
        }
    }
    
    func loadForgotPasswordScreen() {
        
//        let fogotController: BKForgetPasswordViewController = BKCommon.getInstantiatedViewControllerWithIdentifier(identifier: STORYBOARD_ID_FORGOT_PASSWORD_SCREEN, fromStoryBoard: MAIN_STORYBOARD) as! BKForgetPasswordViewController
//
//        fogotController.canShowNavigationBar            = false
//        fogotController.statusBarStyle                  = .light
//        fogotController.viewControllerPresentationStyle = .MODAL
//
//        self.presentViewController(controller: fogotController, isAnimated: true)
    }
        
    func loadHomeScreen() {
        
         if nil == tabBarController{
             tabBarController = ESTabBarController()
         }
         if let tabBar = tabBarController!.tabBar as? ESTabBar {
                tabBar.itemCustomPositioning = .fillIncludeSeparator
            }
        
        let homeViewController = Common.getViewControllerFor(identifier: "rallyHome", storyboardName: .main)
        let homeNavigationController = UINavigationController(rootViewController: homeViewController!)
        let settingsViewController = Common.getViewControllerFor(identifier: "settings", storyboardName: .main) as? UINavigationController
        let config = GMSPlacePickerConfig(viewport: nil)
        let v2 = GMSPlacePickerViewController(config: config)
        
        v2.delegate = self
//        let v2 = Common.getViewControllerFor(identifier: "locationViewController", storyboardName: .main)
//        v2!.view.addSubview(placeController.view)
        
        let tab2NavigationController = UINavigationController(rootViewController: v2)
            homeViewController!.tabBarItem = ESTabBarItem.init(AnimateTipsContentView2(), title: nil, image: UIImage(named: "home_active"), selectedImage: UIImage(named: "home_unactive"))
        v2.tabBarItem = ESTabBarItem.init(AnimateTipsContentView2(), title: nil, image: UIImage(named: "location_active"), selectedImage: UIImage(named: "location_unactive"))
        settingsViewController!.tabBarItem = ESTabBarItem.init(AnimateTipsContentView2.init(specialWithAutoImplies: true), title: nil, image: UIImage(named: "settings_active"), selectedImage: UIImage(named: "settings_unactive"))
            
            tabBarController?.viewControllers = [homeNavigationController, tab2NavigationController, settingsViewController!]
            
           /* if let tabBarItem = v2.tabBarItem as? ESTabBarItem {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2 ) {
                    tabBarItem.badgeValue = "1"
                }
            }*/
            
//            let navigationController = UINavigationController(rootViewController: tabBarController!)
        
//        let home = Common.getViewControllerFor(identifier: "hometab", storyboardName: .main)
        if let wind = getWindow(){
            wind.rootViewController = tabBarController
            wind.makeKeyAndVisible()
        }
    }
    
    func loadAddEventController(_ eventModel:RallyModel? = nil){
        guard let addEvent = Common.getViewControllerFor(identifier: "addevent", storyboardName: .main) as? AddEventController else {
            return
        }
        if let model = eventModel{
            addEvent.rallyModel = eventModel
        }
        addEvent.viewControllerPresentationStyle = .PUSH
        loadScreen(Controller: addEvent, preNavigationBlock: nil, animated: true)
    }
    
    //MARK:- Communicating With External Apps Or Links
    func externalLoadAgreementURL() {
        
        if #available(iOS 10, *) {
            
            UIApplication.shared.open(URL(string: "agreementURL")!, options: [:], completionHandler: nil)
            
        } else {
            
            UIApplication.shared.openURL(URL(string: "agreementURL")!)
        }
    }
    
    //MARK:- Common functions
    
    func loadScreen(Controller ctlr:BaseViewController, preNavigationBlock handler:PreScreenNavigationBlock?, animated anim:Bool) {
        
        handler?(ctlr)
        
        switch ctlr.viewControllerPresentationStyle {
            
        case .PUSH:
            self.pushViewController(controller: ctlr, isAnimated: anim)
        case .MODAL, .TRANSPARENTMODAL:
            self.presentViewController(controller: ctlr, isAnimated: anim)
        case .ROOT:
            
            break
        }
    }
    
    func pushViewController(controller: BaseViewController, isAnimated animated: Bool) {
        
        let appDelegate: AppDelegate =  UIApplication.shared.delegate as! AppDelegate
        controller.viewControllerPresentationStyle = .PUSH
        
        if let revealViewController = self.getTopViewController() as?  UINavigationController {
            revealViewController.pushViewController(controller, animated: animated)
        }  else if let rootViewController: UINavigationController = appDelegate.window?.rootViewController as? UINavigationController {
            controller.viewControllerPresentationStyle = PresentationStyle.PUSH
            rootViewController.pushViewController(controller, animated: animated)
        } else {
            appDelegate.window?.rootViewController = UINavigationController(rootViewController: controller)
        }
    }
    
    func presentViewControllerWithoutNavigation(controller: BaseViewController, isAnimated animated: Bool) {
        
        //        let appDelegate: AppDelegate =  UIApplication.shared.delegate as! AppDelegate
        controller.viewControllerPresentationStyle = PresentationStyle.MODAL
        controller.present(controller, animated: animated, completion: nil)
    }
    
    func presentViewController(controller: BaseViewController, isAnimated animated: Bool) {
        
        let appDelegate: AppDelegate =  UIApplication.shared.delegate as! AppDelegate
        if controller.viewControllerPresentationStyle == .TRANSPARENTMODAL  {
            controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            controller.isTransparentBackground = true
        } else {
            controller.viewControllerPresentationStyle = PresentationStyle.MODAL
        }
        
        let decoratedController: UIViewController = controller
        
        if let rootViewController: UINavigationController = appDelegate.window?.rootViewController as? UINavigationController {
            
            let childNavigationController: UINavigationController = UINavigationController(rootViewController: decoratedController)
            childNavigationController.adaptToBackgroundTransparencyOverPresentation()
            rootViewController.present(childNavigationController, animated: animated, completion: nil)
        } else{
            let navController = UINavigationController(rootViewController: decoratedController)
            navController.adaptToBackgroundTransparencyOverPresentation()
            if appDelegate.window?.rootViewController?.presentedViewController != nil {
                appDelegate.window?.rootViewController?.presentedViewController?.present(navController, animated: animated, completion: nil)
            } else{
                appDelegate.window?.rootViewController?.present(navController, animated: animated, completion: nil)
            }
        }
    }
    
    func getTopViewController() -> UIViewController {
        
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let controller = (appDelegate.window?.rootViewController)!
        var activeController: UIViewController!
        
        if controller.isKind(of:UITabBarController.self) {
            
            let tabBarController: UITabBarController = controller as! UITabBarController
            let nextViewController = tabBarController.selectedViewController!
            if nextViewController.isKind(of:UINavigationController.self) {
                let navController = nextViewController as! UINavigationController
                activeController = self.getTopMostController(navController: navController)
                if let ctrl = activeController.navigationController{
                    activeController = ctrl
                }
            } else {
                activeController = nextViewController
            }
        } else if controller.isKind(of:UINavigationController.self) {
            
            let navController = controller as! UINavigationController
            activeController = self.getTopMostController(navController: navController)
            if let ctrl = activeController.navigationController{
                activeController = ctrl
            }
        } else{
            if let presentedController = controller.presentedViewController {
                activeController = presentedController
            } else{
                activeController = controller
            }
        }
        
        return activeController
    }
    
    func getTopMostController(navController: UINavigationController) -> UIViewController {
        
        var controller: UIViewController!
        
        if let presentedController = navController.topViewController?.presentedViewController {
            
            controller = presentedController
            
        } else {
            
            controller = navController.topViewController
        }
        
        return controller
    }
    
   /* func getSideMenuDecoratedController(_ controller: BKBaseViewController) -> SWRevealViewController {
        
        let sideMenuRootController: SWRevealViewController = SWRevealViewController()
        self.sideControllerRef = sideMenuRootController
        
        let sideMenuOptionsController = SideMenuOptionsViewController(nibName: "SideMenuOptionsViewController", bundle: Bundle.main)
        
        
        //        sideMenuOptionsController.menuItems = self.getSideMenuItems()
        sideMenuOptionsController.delegate = self
        sideMenuRootController.rearViewController = sideMenuOptionsController
        sideMenuRootController.delegate = self
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(sideMenuDidSwipeLeft(recognizer:)))
        leftSwipe.direction = .left
        sideMenuOptionsController.view.addGestureRecognizer(leftSwipe)
        
        sideMenuRootController.frontViewController = UINavigationController(rootViewController: controller)
        
        return sideMenuRootController
    }*/
        
    
    
    private func getWindow()->UIWindow?{
        if let window = UIApplication.shared.delegate!.window, let wind = window{
            return wind
        }
        return nil
    }
}

extension ScreenManager:GMSPlacePickerViewControllerDelegate{
    // GMSPlacePickerViewControllerDelegate and implement this code.
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        print("Place picked \(place)")
        
        let alertController = UIAlertController(title: AppName, message: "Are you aure you want to create a new event at the selected place?", preferredStyle: .alert)
        let actionContinue = UIAlertAction(title: "Continue", style: .default) { (action) in
            let model = RallyModel()
            model.locationName = place.name
            model.location = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
            self.loadAddEventController(model)
        }
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(actionCancel)
        alertController.addAction(actionContinue)
        
        viewController.present(alertController, animated: true, completion: nil)
        /*self.lblName.text = place.name
         self.lblAddress.text = place.formattedAddress?.components(separatedBy: ", ")
         .joined(separator: "\n")
         self.lblLatitude.text = String(place.coordinate.latitude)
         self.lblLongitude.text = String(place.coordinate.longitude)*/
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        
//        viewController.dismiss(animated: true, completion: nil)
    }
}
