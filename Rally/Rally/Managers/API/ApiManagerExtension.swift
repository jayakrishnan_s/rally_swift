//
//  ApiManagerExtension.swift
//  Realtivity
//
//  Created by Zco on 20/04/17.
//  Copyright © 2017 Zco Engineering Dpt. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

extension APIManager {
    
 /*   func httpGetRequest(path: String, parameters: Parameters?, completionHandler: @escaping (DataResponse<JSON>) -> Void) {
        sessionManager.request(Router.get(path: path, parameters: parameters))
            .validate(statusCode: 200..<300)
            .validate(contentType: [Router.HttpHeaderContentJSONEncoded])
            .downloadProgress { _ in
                //print("Progress: \(progress.fractionCompleted)")
            }
            .responseSwiftyJSON(completionHandler: completionHandler)
        
    }
    
    func httpPostRequest(path: String, parameters: Parameters?, completionHandler: @escaping (DataResponse<Any>) -> Void) {
        sessionManager.request(Router.post(path: path, parameters: parameters))
            .validate(statusCode: 200..<300)
            .validate(contentType: [Router.HttpHeaderContentJSONEncoded])
            .downloadProgress { _ in
                //print("Progress: \(progress.fractionCompleted)")
            }
            .responseJSON(completionHandler: completionHandler)
    }*/
    
    func httpGetRequest(path: String, parameters: Parameters?, success:@escaping (JSON) -> Void, failure: @escaping (RTError) -> Void) {
        sessionManager.request(Router.get(path: path, parameters: parameters))
            .validate(statusCode: 200..<300)
            .validate(contentType: [Router.HttpHeaderContentJSONEncoded])
            .downloadProgress { _ in
                //print("Progress: \(progress.fractionCompleted)")
            }
            .responseSwiftyJSON { (response) -> Void in
                if let httpError = response.error {
                    failure(RTError(localizedTitle: "Error", localizedDescription: httpError.localizedDescription, code: RTConstants.defaultAppErrorCode))
                } else {
                    if let value = response.result.value {
                        guard value["ErrorCode"] == 0 else {
                            failure(RTError(localizedTitle: "Error", localizedDescription: value["ErrorMessage"].string ?? "", code: value["ErrorCode"].int ?? RTConstants.defaultAppErrorCode))
                            return
                        }
                        success(value)
                    } else {
                        failure(RTError(localizedTitle: "Error", localizedDescription: "", code: RTConstants.defaultAppErrorCode))
                    }
                }
        }
        
    }
    
    func httpPostJsonRequest(path: String, parameters: Parameters?, success:@escaping (JSON) -> Void, failure: @escaping (RTError) -> Void) -> DataRequest {
        let request = sessionManager.request(Router.postRaw(path: path, parameters: parameters))
            .validate(statusCode: 200..<300)
            .validate(contentType: [Router.HttpHeaderContentJSONEncoded])
            .downloadProgress { _ in
                //print("Progress: \(progress.fractionCompleted)")
            }
            .responseSwiftyJSON { (response) -> Void in
                
                print(response.request ?? "")
                print(response.result)
                if let httpError = response.error {
                    let code = (httpError as NSError).code
                    if code != -999 {
                        failure(RTError(localizedTitle: "Error", localizedDescription: httpError.localizedDescription, code: RTConstants.defaultAppErrorCode))
                    }
                } else {
                    if let value = response.result.value {
                        guard value["ErrorCode"] == 0 else {
                            failure(RTError(localizedTitle: "Error", localizedDescription: value["ErrorMessage"].string ?? "", code: value["ErrorCode"].int ?? RTConstants.defaultAppErrorCode))
                            return
                        }
                        success(value)
                    } else {
                        failure(RTError(localizedTitle: "Error", localizedDescription: "", code: RTConstants.defaultAppErrorCode))
                    }
                }
        }
        return request
    }
    
    func httpPostQueryJsonRequest(path: String, parameters: Parameters?, success:@escaping (JSON) -> Void, failure: @escaping (RTError) -> Void) {
        sessionManager.request(Router.postQuery(path: path, parameters: parameters))
            .validate(statusCode: 200..<300)
            .validate(contentType: [Router.HttpHeaderContentJSONEncoded])
            .downloadProgress { _ in
                //print("Progress: \(progress.fractionCompleted)")
            }
            .responseSwiftyJSON { (response) -> Void in
                
                print(response.request ?? "")
                print(response.result)
                if let httpError = response.error {
                    failure(RTError(localizedTitle: "Error", localizedDescription: httpError.localizedDescription, code: RTConstants.defaultAppErrorCode))
                } else {
                    if let value = response.result.value {
                        guard value["ErrorCode"] == 0 else {
                            failure(RTError(localizedTitle: "Error", localizedDescription: value["ErrorMessage"].string ?? "", code: value["ErrorCode"].int ?? RTConstants.defaultAppErrorCode))
                            return
                        }
                        success(value)
                    } else {
                        failure(RTError(localizedTitle: "Error", localizedDescription: "", code: RTConstants.defaultAppErrorCode))
                    }
                }
        }
    }
    
    func httpDownload(path: String, parameters: Parameters?, destination: URL) {

        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            return (destination, [.removePreviousFile, .createIntermediateDirectories])
        }

        sessionManager.download(Router.download(path: path, parameters: parameters), to: destination)
            .responseData { (responseData) in
            
            }
        .downloadProgress { (progress) in
            print("Progress: \(progress.fractionCompleted)")
        }
    }
    
    func httpUpload(path: String, fileURL: URL, success:@escaping (JSON) -> Void, failure: @escaping (RTError) -> Void) {
        sessionManager.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(fileURL, withName: "")
        }, with: Router.post(path: path, parameters: nil)) { (encodingCompletion) in
            
            switch encodingCompletion {
            case .success( let upload, _, _) :
                upload.responseSwiftyJSON {response in
                    if let value = response.result.value {
                        guard value["ErrorCode"] == 0 else {
                            failure(RTError(localizedTitle: "Error", localizedDescription: value["ErrorMessage"].string ?? "", code: value["ErrorCode"].int ?? RTConstants.defaultAppErrorCode))
                            return
                        }
                        success(value)
                    }
                }
                break
            case .failure(let error):
                failure(RTError(localizedTitle: "Error", localizedDescription: error.localizedDescription, code: RTConstants.defaultAppErrorCode))
                break
            }
        }
        //req.
    }
    
    func httpUpload(path: String, image: UIImage, fileName: String, mimeType: String? = "image/png", success:@escaping (JSON) -> Void, failure: @escaping (RTError) -> Void) {
        sessionManager.upload(multipartFormData: { (multipartFormData) in
            
            if  let imageData = UIImageJPEGRepresentation(image, 1) {
                multipartFormData.append(imageData, withName: "", fileName: fileName, mimeType: mimeType!)
            }
        
        }, with: Router.post(path: path, parameters: nil)) { (encodingCompletion) in
            
            switch encodingCompletion {
            case .success( let upload, _, _) :
                upload.responseSwiftyJSON {response in
                    if let value = response.result.value {
                        guard value["ErrorCode"] == 0 else {
                            failure(RTError(localizedTitle: "Error", localizedDescription: value["ErrorMessage"].string ?? "", code: value["ErrorCode"].int ?? RTConstants.defaultAppErrorCode))
                            return
                        }
                        success(value)
                    }
                }
                break
            case .failure(let error):
                    failure(RTError(localizedTitle: "Error", localizedDescription: error.localizedDescription, code: RTConstants.defaultAppErrorCode))
                break
            }
        }
    }

   /* func httpPostJSONRequest(path: String, parameters: Parameters?, completionHandler: @escaping (DataResponse<JSON>) -> Void) {
        sessionManager.request(Router.postRaw(path: path, parameters: parameters))
            .validate(statusCode: 200..<300)
            .validate(contentType: [Router.HttpHeaderContentJSONEncoded])
            .downloadProgress { _ in
                //print("Progress: \(progress.fractionCompleted)")
            }
            .responseSwiftyJSON(completionHandler: completionHandler)
    }
    
    func httpDeleteRequest(path: String, parameters: Parameters?, completionHandler: @escaping (DataResponse<Any>) -> Void) {
        sessionManager.request(Router.delete(path: path))
            .validate(statusCode: 200..<300)
            .validate(contentType: [Router.HttpHeaderContentJSONEncoded])
            .downloadProgress { _ in
               // print("Progress: \(progress.fractionCompleted)")
            }
            .responseJSON(completionHandler: completionHandler)
    }
    
    func httpPutRequest(path: String, parameters: Parameters?, completionHandler: @escaping (DataResponse<Any>) -> Void) {
        sessionManager.request(Router.put(path: path, parameters: parameters))
            .validate(statusCode: 200..<300)
            .validate(contentType: [Router.HttpHeaderContentJSONEncoded])
            .downloadProgress { _ in
                //print("Progress: \(progress.fractionCompleted)")
            }
            .responseJSON(completionHandler: completionHandler)
    }
    
    func httpPutJSONRequest(path: String, parameters: Parameters?, completionHandler: @escaping (DataResponse<Any>) -> Void) {
        sessionManager.request(Router.putRaw(path: path, parameters: parameters))
            .validate(statusCode: 200..<300)
            .validate(contentType: [Router.HttpHeaderContentJSONEncoded])
            .downloadProgress { _ in
                //print("Progress: \(progress.fractionCompleted)")
            }
            .responseJSON(completionHandler: completionHandler)
    }*/
}
