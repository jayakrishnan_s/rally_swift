//
//  ApiManager.swift
//  Realtivity
//
//  Copyright © 2017 Zco Corporation. All rights reserved.
//

import Foundation
import Alamofire
//import AlamofireObjectMapper
import ObjectMapper

class APIManager {
    
    var sessionManager: SessionManager {
        get {
            return Alamofire.SessionManager.default
        }
    }
    
    var inProgress = false
    
    enum Router: URLRequestConvertible {
        
        case get(path: String, parameters: Parameters?)
        case post(path: String, parameters: Parameters?)
        case postRaw(path: String, parameters: Parameters?)
        case delete(path: String)
        case put(path: String, parameters: Parameters?)
        case putRaw(path: String, parameters: Parameters?)
        case postQuery(path: String, parameters: Parameters?)
        case download(path: String, parameters: Parameters?)
        
        static let baseURLString = "http://10.1.1.18:8107/api/" // local
    //    static let baseURLString = "http://10.1.1.37:8093/api/"// performance
        
        // static let baseURLString = "http://35.162.54.12:8082/api/"
        
        //static let baseURLString = "http://35.162.54.12:8086/api/"
        
        static let baseURLTCString = ""
        static var HttpHeaderAcceptJSON = "application/json"
        static var HttpHeaderContentUrlEncoded = "application/x-www-form-urlencoded"
        static var HttpHeaderContentJSONEncoded = "application/json"
        
        var method: HTTPMethod {
            switch self {
            case .get:
                return .get
            case .post:
                return .post
            case .delete:
                return .delete
            case .put:
                return .put
            case .postRaw:
                return .post
            case .postQuery:
                return .post
            case .putRaw:
                return .put
            case .download:
                return .get
            }
        }
        
        func asURLRequest() throws -> URLRequest {
            let url = try Router.baseURLString.asURL()
            
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = method.rawValue
            
            var headers: HTTPHeaders
            if let existingHeaders = urlRequest.allHTTPHeaderFields {
                headers = existingHeaders
            } else {
                headers = HTTPHeaders()
            }
            headers["Accept"] = Router.HttpHeaderAcceptJSON
            headers["Content-Type"] = Router.HttpHeaderContentUrlEncoded
            urlRequest.allHTTPHeaderFields = headers
            urlRequest.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
            
            urlRequest.setValue("Bearer " + (RTCommon.sessionToken() ?? ""), forHTTPHeaderField: "Authorization")
            urlRequest.setValue(RTCommon.deviceIdentifier(), forHTTPHeaderField: "DeviceID")
            urlRequest.setValue(RTCommon.bundleVersion(), forHTTPHeaderField: "AppVersion")
            urlRequest.setValue(RTCommon.osVersion(), forHTTPHeaderField: "OSVersion")
        
            loadCookies()
            
            switch self {
            case .get(let path, let parameters), .post(let path, let parameters), .download(let path, let parameters):
                urlRequest.url?.appendPathComponent(path)
                urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
            case .delete(let path):
               urlRequest.url?.appendPathComponent(path)
                urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
                
            case .postRaw(let path, let parameters), .putRaw(let path, let parameters):
                urlRequest.url?.appendPathComponent(path)
                urlRequest.httpBody = nil != parameters ? try JSONSerialization.data(withJSONObject: parameters as Any) : nil
                urlRequest.setValue(Router.HttpHeaderContentJSONEncoded, forHTTPHeaderField: "Content-Type")
                
            case .postQuery(let path, let parameters):
                urlRequest.url?.appendPathComponent(path)
                urlRequest = try URLEncoding.queryString.encode(urlRequest, with: parameters)
                
            case .put(let path, let parameters):
                urlRequest.url?.appendPathComponent(path)
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters as Any)
                urlRequest.setValue(Router.HttpHeaderContentJSONEncoded, forHTTPHeaderField: "Content-Type")

           }
            
            return urlRequest
        }
        
        func loadCookies() {
            guard let cookieArray = UserDefaults.standard.array(forKey: "savedCookies") as? [[HTTPCookiePropertyKey: Any]]
                else {
                    return
            }
            for cookieProperties in cookieArray {
                if let cookie = HTTPCookie(properties: cookieProperties) {
                    HTTPCookieStorage.shared.setCookie(cookie)
                }
            }
        }
    }
    
    func httpGetRequest<T: BaseMappable>(path: String, parameters: Parameters?, completionHandler: @escaping (DataResponse<T>) -> Void) {
            sessionManager.request(Router.get(path: path, parameters: parameters))
                .validate(statusCode: 200..<300)
                .validate(contentType: [Router.HttpHeaderContentJSONEncoded])
                .downloadProgress { _ in
                    //print("Progress: \(progress.fractionCompleted)")
                }
                .responseObject(completionHandler: completionHandler)
    }
    
    func httpGetRequest<T: BaseMappable>(path: String, parameters: Parameters?, completionHandler: @escaping (DataResponse<[T]>) -> Void) {
            sessionManager.request(Router.get(path: path, parameters: parameters))
                .validate(statusCode: 200..<300)
                .validate(contentType: [Router.HttpHeaderContentJSONEncoded])
                .downloadProgress {_ in
                    //print("Progress: \(progress.fractionCompleted)")
                }
                .responseArray(completionHandler: completionHandler)
    }
    
    func httpPostRequest<T: BaseMappable>(path: String, parameters: Parameters?, completionHandler: @escaping (DataResponse<T>) -> Void) {
            sessionManager.request(Router.post(path: path, parameters: parameters))
                .validate(statusCode: 200..<300)
                .validate(contentType: [Router.HttpHeaderContentJSONEncoded])
                .downloadProgress {_ in
                    //print("Progress: \(progress.fractionCompleted)")
                }
                .responseObject(completionHandler: completionHandler)
    }
    
    /*func httpPostQueryStringRequest<T: BaseMappable>(path: String, parameters: Parameters?, completionHandler: @escaping (DataResponse<T>) -> Void) {
        sessionManager.request(Router.post(path: path, parameters: parameters))
            .validate(statusCode: 200..<300)
            .validate(contentType: [Router.HttpHeaderContentJSONEncoded])
            .downloadProgress {_ in
                //print("Progress: \(progress.fractionCompleted)")
            }
            .responseObject(completionHandler: completionHandler)
    }*/
    
    func httpDeleteRequest<T: BaseMappable>(path: String, parameters: Parameters?, completionHandler: @escaping (DataResponse<T>) -> Void) {
            sessionManager.request(Router.delete(path: path))
                .validate(statusCode: 200..<300)
                .validate(contentType: [Router.HttpHeaderContentJSONEncoded])
                .downloadProgress { _ in
                    //print("Progress: \(progress.fractionCompleted)")
                }
                .responseObject(completionHandler: completionHandler)
    }

    func httpDeleteRequest(path: String, parameters: Parameters?, completionHandler: @escaping (DefaultDataResponse) -> Void) {
        sessionManager.request(Router.delete(path: path))
            .validate(statusCode: 200..<300)
            .validate(contentType: [Router.HttpHeaderContentJSONEncoded])
            .downloadProgress { _ in
                //print("Progress: \(progress.fractionCompleted)")
            }
            .response(completionHandler: { response in
                completionHandler(response)
            })
    }
    
    func httpPutRequest<T: BaseMappable>(path: String, parameters: Parameters?, completionHandler: @escaping (DataResponse<T>) -> Void) {
        sessionManager.request(Router.put(path: path, parameters: parameters))
            .validate(statusCode: 200..<300)
            .validate(contentType: [Router.HttpHeaderContentJSONEncoded])
            .downloadProgress { _ in
                //print("Progress: \(progress.fractionCompleted)")
            }
            .responseObject(completionHandler: completionHandler)
    }
}
