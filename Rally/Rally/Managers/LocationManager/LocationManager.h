//
//  LocationManager.h
//  Cabo Driver
//
//  Created by Jayakrishnan.S on 21/05/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

#ifndef LocationManager_h
#define LocationManager_h

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define FENCE_EVENT_TYPE_KEY @" eventType"
#define FENCE_EVENT_TIMESTAMP_KEY @" eventTimeStamp"
#define FENCE_IDENTIFIER_KEY @" fenceIDentifier"
#define FENCE_COORDINATE_KEY @" fenceCoordinate"

#define GET_FENCE_EVENT_STRING(fenceEventTypeEnum) [@[ @" FenceEventAdded", @" FenceEventRemoved",@" FenceEventFailed", @" FenceEventRepeated", @" FenceEventEnterFence",@" FenceEventExitFence", @" FenceEventNone"] objectAtIndex:(fenceEventTypeEnum)]

typedef enum : NSUInteger {
     FenceEventTypeAdded,
     FenceEventTypeRemoved,
     FenceEventTypeFailed,
     FenceEventTypeRepeated,
     FenceEventTypeEnterFence,
     FenceEventTypeExitFence,
     FenceEventTypeNone
}  FenceEventType;

@interface FenceInfo : NSObject


@property(nonatomic, assign) NSString *eventType;
@property(nonatomic, strong) NSString *eventTimeStamp;
@property(nonatomic, strong) NSString *fenceIDentifier;
@property(nonatomic, strong) NSDictionary *fenceCoordinate;//uses  LATITUDE,   LONGITUDE,  RADIOUS to wrap data

- (NSDictionary*)dictionary;



@end


#define  DEFAULT_FENCE_RADIOUS 100.0f
#define  LATITUDE @"latitude"
#define  LONGITUDE @"longitude"
#define  SPEED @"speed"
#define  ALTITUDE @"altitude"
#define  RADIOUS @" Radious"

#define  ADDRESS_NAME @"address_name"         // eg. Apple Inc.
#define  ADDRESS_STREET @"address_street"     // street name, eg. Infinite Loop
#define  ADDRESS_CITY @"address_city"         // city, eg. Cupertino
#define  ADDRESS_STATE @"address_state"       // state, eg. CA
#define  ADDRESS_COUNTY @"address_county"     // county, eg. Santa Clara
#define  ADDRESS_ZIPCODE @"address_zipcode"   // zip code, eg. 95014
#define  ADDRESS_COUNTRY @"address_country"   // eg. United States
#define  ADDRESS_DICTIONARY @"address_full_dictionary"  //total "addressDictionary" of "CLPlacemark" object


typedef void(^LocationUpdateBlock)(BOOL success, NSDictionary *locationDictionary, NSError *error);
typedef void(^GeoCodeUpdateBlock)(BOOL success, NSDictionary *geoCodeDictionary, NSError *error);

@protocol LocationManagerDelegate <NSObject>

/**
 *   Gives an FenceInfo Object of the Fence which just added
 */
-(void)LocationManagerDidAddFence:(FenceInfo *)fenceInfo;


/**
 *   Gives an FenceInfo Object of the Fence which just failed to monitor
 */
-(void)LocationManagerDidFailedFence:(FenceInfo *)fenceInfo;


/**
 *   Gives an FenceInfo Object of a Fence just entered
 */
-(void)LocationManagerDidEnterFence:(FenceInfo *)fenceInfo;


/**
 *   Gives an FenceInfo Object of a Exited Fence
 */
-(void)LocationManagerDidExitFence:(FenceInfo *)fenceInfo;


/**
 *   Gives an Location Dictionary using keys LATITUDE, LONGITUDE, ALTITUDE
 */
-(void)LocationManagerDidUpdateLocation:(NSDictionary *)latLongAltitudeDictionary;


/**
 *   Gives an Dictionary using current geocode or adress information with  ADDRESS_* keys
 */
-(void)LocationManagerDidUpdateGeocodeAdress:(NSDictionary *)addressDictionary;


@end

@interface LocationManager : NSObject
{
    
    __weak id <LocationManagerDelegate> _delegate;
}

/**
 *  The delegate, using this the location events are fired.
 */
@property (nonatomic, weak) id <LocationManagerDelegate> delegate;

/**
 *  The last known Geocode address determinded, will be nil if there is no geocode was requested.
 */
@property (nonatomic, strong) NSDictionary *lastKnownGeocodeAddress;

/**
 *  The last known location received. Will be nil until a location has been received. Returns an Dictionary using keys  LATITUDE,  LONGITUDE,  ALTITUDE
 */
@property (nonatomic, strong) NSDictionary *lastKnownGeoLocation;

/**
 *  Similar to lastKnownLocation, The last location received. Will be nil until a location has been received. Returns an Dictionary using keys  LATITUDE,  LONGITUDE,  ALTITUDE
 */
@property (nonatomic, strong) NSDictionary *location;

/**
 *   The desired location accuracy in meters. Default is 100 meters.
 *<p>
 *The location service will try its best to achieve
 your desired accuracy. However, it is not guaranteed. To optimize
 power performance, be sure to specify an appropriate accuracy for your usage scenario (eg, use a large accuracy value when only a coarse location is needed). Set it to 0 to achieve the best possible accuracy.
 *</p>
 */
@property(nonatomic, assign) double desiredAcuracy;

/**
 *  Specifies the minimum update distance in meters. Client will not be notified of movements of less
 than the stated value, unless the accuracy has improved. Pass in 0 to be
 notified of all movements. By default, 100 meters is used.
 */
@property(nonatomic, assign) double distanceFilter;

/**
 *   Returns a singeton(static) instance of the LocationManager
 *   <p>
 *   Returns a singeton(static) instance of the LocationManager
 *   </p>
 *   @return singeton(static) instance of the LocationManager
 */
+ (instancetype)sharedManager;


/**
 *   Returns location permission status
 *   <p>
 *   Returns wheather location is permitted or not by the user
 *   </p>
 *   @return true or false based on permission given or not
 */
+ (BOOL)locationPermission;

/**
 *   Prompts user for location permission
 *   <p>
 *   If user havn't seen any permission requests yet, calling this method will ask user for location permission
 *   For knowing permission status, call the @locationPermission method
 *   </p>
 *
 *
 */
-(void)getPermissionForStartUpdatingLocation;

/**
 *   Gives an Array of dictionary formatted FenceInfo which are currently active
 *   <p>
 *   Gives an Array of dictionary formatted FenceInfo which are currently active
 *   </p>
 *
 *   @return an Array of dictionary formatted FenceInfo
 */
- (NSArray*)getCurrentFences;

/**
 *   Delete all the fences which are currently active.
 *   <p>
 *   Those fences we created and are currently active, delete all of'em.
 *   </p>
 *
 *
 */
-(void)deleteCurrentFences;

/**
 *   Returns current location through the LocationManagerDelegate, can be adjusted using the desiredAcuracy and distanceFilter properties.
 *   <p>
 *   Gives location of device using delegate LocationManagerDidUpdateLocation:
 *   </p>
 *   @param delegate where the location will be delivered, which implements LocationManagerDelegate
 *
 */
- (void)getCurrentLocationWithDelegate:(id)delegate;


/**
 *   Returns current location, can be adjusted using the desiredAcuracy and distanceFilter properties.
 *   <p>
 *   Gives location of device using the completion block
 *   </p>
 *   @param completion block which will be called when the location is updated
 *
 */
- (void)getCurrentLocationWithCompletion:(LocationUpdateBlock)completion;


/**
 *   Returns current location's geocode address
 *   <p>
 *   Gives the currents location's geocode addres using LocationManagerDelegate, uses Apple's own geocode API to get teh current address
 *   </p>
 *
 */
- (void)getCurrentGeocodeAddressWithDelegate:(id)delegate;

/**
 *   Returns current location's geocode address
 *   <p>
 *   Gives the currents location's geocode addres using given block, uses Apple's own geocode API to get teh current address
 *   </p>
 *
 */
- (void)getCurrentGeoCodeAddressWithCompletion:(GeoCodeUpdateBlock)completion;


/**
 *   Returns current location continiously through LocationManagerDidUpdateLocation method, can be adjusted using the desiredAcuracy and distanceFilter properties.
 *   <p>
 *   Gives the current location continiously until the -stopGettingLocation is called
 *   </p>
 *
 */
-(void)getContiniousLocationWithDelegate:(id)delegate;

/**
 *   Start monitoring significant location changes.  The behavior of this service is not affected by the desiredAccuracy
 or distanceFilter properties. Returns location if user's is moved significantly, through LocationManagerDidUpdateLocation delegate call. Gives the significant location change continiously until the -stopGettingLocation is called
 *  <p>
 *  Apps can expect a notification as soon as the device moves 500 meters or more from its previous notification. It should not expect notifications more frequently than once every 5 minutes. If the device is able to retrieve data from the network, the location manager is much more likely to deliver notifications in a timely manner. (from Apple Doc)
 *  </p>
 */
-(void)getSingificantLocationChangeWithDelegate:(id)delegate;


/**
 *   Stops updating location for Continious or Significant changes
 *   <p>
 *   Use this method to stop accessing and getting the location data continiously. If you've called -getContiniousLocationWithDelegate: or -getSingificantLocationChangeWithDelegate: method before, call -stopGettingLocation method to stop that.
 *   </p>
 */
-(void)stopGettingLocation;

/**
 *   Adds a geofence at the current location
 *   <p>
 *   First updates current location of the device, and then add it as a Geofence. Optionally also tries to determine the Geocode/Address. Default radios of the fence is set to 100 meters
 *   </p>
 *   <p>
 *   Checks if there is already a fence exists in this coordinate, if so, fires delegate LocationManagerDidAddFence: with a  FenceEventTypeRepeated event.
 *   </p>
 *   @warning When using this method for adding multiple fence at once, reverse geocoding method may fail for too many request in small amount of time.
 *
 */
- (void)addGeofenceAtCurrentLocation;

/**
 *   Adds a geofence at the current location with a radious
 *   <p>
 *   First updates current location of the device, and then add it as a Geofence. Optionally also tries to determine the Geocode/Address. Also sets the radious of the fence with given value
 *   </p>
 *   <p>
 *   Checks if there is already a fence exists in this coordinate, if so, fires delegate LocationManagerDidAddFence: with a  FenceEventTypeRepeated event.
 *   </p>
 *   @warning When using this method for adding multiple fence at once, reverse geocoding method may fail for too many request in small amount of time.
 *   @param radious The radious for the fence
 *
 */
- (void)addGeofenceAtCurrentLocationWithRadious:(CLLocationDistance)radious;

/**
 *   Adds a geofence at given latitude/longitude, radious and indentifer.
 *   <p>
 *   Checks if there is already a fence exists in this coordinate, if so, fires delegate LocationManagerDidAddFence: with a  FenceEventTypeRepeated event.
 *   </p>
 *   @warning When using this method for adding multiple fence at once, always deliver Identifier value, otherwise the reverse geocoding method may fail for too many request in small amount of time.
 *   @param latitude The latitude where to add the fence
 *   @param longitude The longitude where to add the fence
 *   @param radious The radious for the fence
 *   @param identifier The name of the fence. If the indentifier is nil, this method will try to use geocode to determine the address of this coordinate and use it as identifer. WARNING: When using this method for adding multiple fence at once, always deliver Identifier value
 *
 */

- (void)addGeofenceAtlatitude:(double)latitude andLongitude:(double)longitude withRadious:(double)radious withIdentifier:(NSString*)identifier;


/**
 *   Adds a geofence at given latitude/longitude, radious and indentifer.
 *   <p>
 *   Checks if there is already a fence exists in this coordinate, if so, fires delegate LocationManagerDidAddFence: with a  FenceEventTypeRepeated event.
 *   </p>
 *   @warning When using this method for adding multiple fence at once, always deliver Identifier value, otherwise the reverse geocoding method may fail for too many request in small amount of time.
 *   @param radious The radious for the fence
 *   @param identifier The name of the fence. If the indentifier is nil, this method will try to use geocode to determine the address of this coordinate and use it as identifer. WARNING: When using this method for adding multiple fence at once, always deliver Identifier value

 */
- (void)addGeofenceAtCurrentLocationWithRadious:(CLLocationDistance)radious withIdentifier:(NSString*)identifier;

/**
 *   Adds a geofence at given coordinate, radious and indentifer.
 *   <p>
 *   Checks if there is already a fence exists in this coordinate, if so, fires delegate LocationManagerDidAddFence: with a  FenceEventTypeRepeated event.
 *   </p>
 *   @warning When using this method for adding multiple fence at once, always deliver Identifier value, otherwise the reverse geocoding method may fail for too many request in small amount of time.
 *   @param coordinate The coordinate as CLLocationCoordinate2D where to add the fence
 *   @param radious The radious for the fence
 *   @param identifier The name of the fence. If the indentifier is nil, this method will try to use geocode to determine the address of this coordinate and use it as identifer. WARNING: When using this method for adding multiple fence at once, always deliver Identifier value
 */
- (void)addGeofenceAtCoordinates:(CLLocationCoordinate2D)coordinate withRadious:(CLLocationDistance)radious withIdentifier:(NSString*)identifier;

/**
 *   Adds a geofence at given location, radious and indentifer.
 *   <p>
 *   Checks if there is already a fence exists in this location, if so, fires delegate LocationManagerDidAddFence: with a  FenceEventTypeRepeated event.
 *   </p>
 *   @warning When using this method for adding multiple fence at once, always deliver Identifier value, otherwise the reverse geocoding method may fail for too many request in small amount of time.
 *   @param location The location where to add the fence
 *   @param radious The radious for the fence
 *   @param identifier The name of the fence. If the indentifier is nil, this method will try to use geocode to determine the address of this coordinate and use it as identifer. WARNING: When using this method for adding multiple fence at once, always deliver Identifier value
 */
- (void)addGeofenceAtLocation:(CLLocation*)location withRadious:(CLLocationDistance)radious withIdentifier:(NSString*)identifier;

/**
 *   Adds a geofence at a location, radious and indentifer using the FenceInfo object
 *   <p>
 *   Checks if there is already a fence exists in this location, if so, fires delegate LocationManagerDidAddFence: with a  FenceEventTypeRepeated event.
 *   </p>
 *   @warning When using this method for adding multiple fence at once, always deliver Identifier value, otherwise the reverse geocoding method may fail for too many request in small amount of time.
 *   @param fenceInfo The location where to add the fence
 */

-(void)addGeoFenceUsingFenceInfo:(FenceInfo*)fenceInfo;


/**
 *   Deletes a geofence at a location, using the FenceInfo object
 *   <p>
 *   It searches for the  identifiers of the added fences based on fenceInfo, and deletes the desired one.
 *   </p>
 *   @param fenceInfo The location where to add the fence
 */

-(void)deleteGeoFence:(FenceInfo*)fenceInfo;

/**
 *   Deletes a geofence with a identifier
 *   <p>
 *   It searches for the  identifiers of the added fences, and deletes the desired one.
 *   </p>
 *   @param identifier The identifier of the geofence need to be deleted
 */
-(void)deleteGeoFenceWithIdentifier:(NSString*)identifier;

@end
#endif /* LocationManager_h */
