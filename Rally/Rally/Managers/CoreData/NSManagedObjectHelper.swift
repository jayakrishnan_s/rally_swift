//
//  NSManagedObjectHelper.swift
//  Rally
//
//  Created by Jayakrishnan S on 10/10/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import Foundation
import CoreData

fileprivate let noFetchLimit = -1
fileprivate let defaultFetchOffset = 0
fileprivate let defaultFetchBatchSize = 0

protocol NSManagedObjectEasyAccessProtocol {
}

extension NSManagedObjectEasyAccessProtocol where Self: NSManagedObject {
    // MARK: - Methods for creating an object
    
    static func createObject() -> Self? {
        guard let managedObjectContext = CoreDataManager.sharedInstance.managedObjectContext else {
            return nil
        }
        
        return createObject(inContext: managedObjectContext)
    }
    
    static func createObject(inContext context: NSManagedObjectContext) -> Self? {
        return NSEntityDescription.insertNewObject(forEntityName: String(describing: self),
                                                   into: context) as? Self
    }
    
    // MARK: - Methods for fetching an object
    
    static func fetchObject() -> Self? {
        guard let managedObjectContext = CoreDataManager.sharedInstance.managedObjectContext else {
            return nil
        }
        
        return fetchObject(inContext: managedObjectContext)
    }
    
    static func fetchObject(inContext context: NSManagedObjectContext) -> Self? {
        return fetchObjectList(withPredicate: nil, sortDescriptors: nil, fetchLimit: 1,
                               fetchOffset: defaultFetchOffset, fetchBatchSize: defaultFetchBatchSize,
                               inContext: context)?.first
    }
    
    static func fetchObject(withAttributeName name: String, attributeValue value: Any) -> Self? {
        guard let managedObjectContext = CoreDataManager.sharedInstance.managedObjectContext else {
            return nil
        }
        
        return fetchObject(withAttributeName: name, attributeValue: value, inContext: managedObjectContext)
    }
    
    static func fetchObject(withAttributeName name: String, attributeValue value: Any,
                            inContext context: NSManagedObjectContext) -> Self? {
        return fetchObjectList(withAttributeName: name, attributeValue: value, fetchLimit: 1,
                               inContext: context)?.first
    }
    
    static func fetchObject(withPredicate predicate: NSPredicate) -> Self? {
        guard let managedObjectContext = CoreDataManager.sharedInstance.managedObjectContext else {
            return nil
        }
        
        return fetchObject(withPredicate: predicate, inContext: managedObjectContext)
    }
    
    static func fetchObject(withPredicate predicate: NSPredicate, inContext context: NSManagedObjectContext) -> Self? {
        return fetchObjectList(withPredicate: predicate, sortDescriptors: nil, fetchLimit: 1,
                               fetchOffset: defaultFetchOffset, fetchBatchSize: defaultFetchBatchSize,
                               inContext: context)?.first
    }
    
    // MARK: - Methods for fetching object list
    
    static func fetchObjectList() -> [Self]? {
        guard let managedObjectContext = CoreDataManager.sharedInstance.managedObjectContext else {
            return nil
        }
        
        return fetchObjectList(inContext: managedObjectContext)
    }
    
    static func fetchObjectList(inContext context: NSManagedObjectContext) -> [Self]? {
        return fetchObjectList(withFetchLimit: noFetchLimit, inContext: context)
    }
    
    static func fetchObjectList(withFetchLimit limit: NSInteger, inContext context: NSManagedObjectContext) -> [Self]? {
        return fetchObjectList(withPredicate: nil, sortDescriptors: nil, fetchLimit: limit,
                               fetchOffset: defaultFetchOffset, fetchBatchSize: defaultFetchBatchSize,
                               inContext: context)
    }
    
    static func fetchObjectList(withAttributeName name: String, attributeValue value: Any) -> [Self]? {
        guard let managedObjectContext = CoreDataManager.sharedInstance.managedObjectContext else {
            return nil
        }
        
        return fetchObjectList(withAttributeName: name, attributeValue: value, inContext: managedObjectContext)
    }
    
    static func fetchObjectList(withAttributeName name: String, attributeValue value: Any,
                                inContext context: NSManagedObjectContext) -> [Self]? {
        return fetchObjectList(withAttributeName: name, attributeValue: value, fetchLimit: noFetchLimit,
                               inContext: context)
    }
    
    static func fetchObjectList(withAttributeName name: String, attributeValue value: Any, fetchLimit limit: NSInteger,
                                inContext context: NSManagedObjectContext) -> [Self]? {
        var predicate: NSPredicate?
        if let attrValue = value as? String {
            predicate = NSPredicate(format: "%K == %@", name, attrValue)
        } else if let attrValue = value as? NSNumber {
            predicate = NSPredicate(format: "%K == %@", name, attrValue)
        }
        
        return fetchObjectList(withPredicate: predicate, sortDescriptors: nil, fetchLimit: limit,
                               fetchOffset: defaultFetchOffset, fetchBatchSize: defaultFetchBatchSize,
                               inContext: context)
    }
    
    static func fetchObjectList(withPredicate predicate: NSPredicate) -> [Self]? {
        guard let managedObjectContext = CoreDataManager.sharedInstance.managedObjectContext else {
            return nil
        }
        
        return fetchObjectList(withPredicate: predicate, inContext: managedObjectContext)
    }
    
    static func fetchObjectList(withPredicate predicate: NSPredicate,
                                inContext context: NSManagedObjectContext) -> [Self]? {
        return fetchObjectList(withPredicate: predicate, sortDescriptors: nil, fetchLimit: noFetchLimit,
                               fetchOffset: defaultFetchOffset, fetchBatchSize: defaultFetchBatchSize,
                               inContext: context)
    }
    
    static func fetchObjectList(withPredicate predicate: NSPredicate?,
                                sortDescriptors descriptors: [NSSortDescriptor]?) -> [Self]? {
        guard let managedObjectContext = CoreDataManager.sharedInstance.managedObjectContext else {
            return nil
        }
        return fetchObjectList(withPredicate: predicate, sortDescriptors: descriptors, fetchLimit: noFetchLimit,
                               fetchOffset: defaultFetchOffset, fetchBatchSize: defaultFetchBatchSize,
                               inContext: managedObjectContext)
    }
    
    static func fetchObjectList(withPredicate predicate: NSPredicate?,
                                sortDescriptors descriptors: [NSSortDescriptor]?, inContext context: NSManagedObjectContext) -> [Self]? {
        
        return fetchObjectList(withPredicate: predicate, sortDescriptors: descriptors, fetchLimit: noFetchLimit,
                               fetchOffset: defaultFetchOffset, fetchBatchSize: defaultFetchBatchSize,
                               inContext: context)
    }
    
    static func fetchObjectList(withPRedicate predicate: NSPredicate?, sortDescriptors descriptors: [NSSortDescriptor]?, fetchLimit: Int) -> [Self]? {
        guard let managedObjectContext = CoreDataManager.sharedInstance.managedObjectContext else {
            return nil
        }
        
        return fetchObjectList(withPredicate: predicate, sortDescriptors: descriptors, fetchLimit: fetchLimit,
                               fetchOffset: defaultFetchOffset, fetchBatchSize: defaultFetchBatchSize,
                               inContext: managedObjectContext)
    }
    
    static func fetchObjectList(withPredicate predicate: NSPredicate?, sortDescriptors descriptors: [NSSortDescriptor]?,
                                fetchLimit limit: NSInteger, fetchOffset offset: NSInteger)  -> [Self]? {
        guard let managedObjectContext = CoreDataManager.sharedInstance.managedObjectContext else {
            return nil
        }
        
        return fetchObjectList(withPredicate: predicate, sortDescriptors: descriptors, fetchLimit: limit, fetchOffset: offset, fetchBatchSize: defaultFetchBatchSize, inContext: managedObjectContext)
    }
    
    // swiftlint:disable:next function_parameter_count
    static func fetchObjectList(withPredicate predicate: NSPredicate?, sortDescriptors descriptors: [NSSortDescriptor]?,
                                fetchLimit limit: NSInteger, fetchOffset offset: NSInteger, fetchBatchSize batchSize: NSInteger,
                                inContext context: NSManagedObjectContext) -> [Self]? {
        var request: NSFetchRequest<Self>
        if #available(iOS 10.0, *) {
            guard let fetchRequest = Self.fetchRequest() as? NSFetchRequest<Self> else {
                return nil
            }
            request = fetchRequest
        } else {
            request = NSFetchRequest(entityName: String(describing: self))
        }
        
        request.includesSubentities = false
        
        if let predicate = predicate {
            request.predicate = predicate
        }
        
        if let sortDescriptors = descriptors {
            request.sortDescriptors = sortDescriptors
        }
        
        if noFetchLimit != limit {
            request.fetchLimit = limit
        }
        
        request.fetchOffset = offset
        request.fetchBatchSize = batchSize
        
        return fetchObjectList(withFetchRequest: request, inContext: context)
    }
    
    static func fetchObjectList(withFetchRequest fetchRequest: NSFetchRequest<Self>,
                                inContext context: NSManagedObjectContext) -> [Self]? {
        assertCondition(for: context)
        var result: [NSManagedObject]?
        context.performAndWait {
            do {
                result = try context.fetch(fetchRequest)
            } catch {
                if let error = error as NSError? {
                    print("Unresolved error \(error), \(error.userInfo)")
                }
            }
        }
        
        if let result = result as? [Self] {
            return (!result.isEmpty) ? result : nil
        } else {
            return nil
        }
    }
    
    // MARK: - Methods for fetching objects count
    
    static func countOfAllObjects() -> NSInteger {
        guard let managedObjectContext = CoreDataManager.sharedInstance.managedObjectContext else {
            return 0
        }
        
        return countOfAllObjects(inContext: managedObjectContext)
    }
    
    static func countOfAllObjects(inContext context: NSManagedObjectContext) -> NSInteger {
        return countOfAllObjects(withPredicate: nil, inContext: context)
    }
    
    static func countOfAllObjects(withAttributeName name: String,
                                  attributeValue value: AnyObject) -> NSInteger {
        guard let managedObjectContext = CoreDataManager.sharedInstance.managedObjectContext else {
            return 0
        }
        
        return countOfAllObjects(withAttributeName: name, attributeValue: value, inContext: managedObjectContext)
    }
    
    static func countOfAllObjects(withAttributeName name: String,
                                  attributeValue value: AnyObject,
                                  inContext context: NSManagedObjectContext) -> NSInteger {
        var predicate: NSPredicate?
        if let attrValue = value as? String {
            predicate = NSPredicate(format: "%K == %@", name, attrValue)
        } else if let attrValue = value as? NSNumber {
            predicate = NSPredicate(format: "%K == %@", name, attrValue)
        }
        
        return countOfAllObjects(withPredicate: predicate, inContext: context)
    }
    
    static func countOfAllObjects(withPredicate predicate: NSPredicate?) -> NSInteger {
        guard let managedObjectContext = CoreDataManager.sharedInstance.managedObjectContext else {
            return 0
        }
        
        return countOfAllObjects(withPredicate: predicate, inContext: managedObjectContext)
    }
    
    static func countOfAllObjects(withPredicate predicate: NSPredicate?,
                                  inContext context: NSManagedObjectContext) -> NSInteger {
        assertCondition(for: context)
        
        var request: NSFetchRequest<Self>
        if #available(iOS 10.0, *) {
            guard let fetchRequest = Self.fetchRequest() as? NSFetchRequest<Self> else {
                return 0
            }
            request = fetchRequest
        } else {
            request = NSFetchRequest(entityName: String(describing: self))
        }
        
        request.includesSubentities = false
        
        if let predicate = predicate {
            request.predicate = predicate
        }
        
        var count: Int = 0
        
        context.performAndWait {
            do {
                count = try context.count(for: request)
            } catch {
                if let error = error as NSError? {
                    print("Unresolved error \(error), \(error.userInfo)")
                }
            }
        }
        
        return count
    }
    
    // MARK: - Methods for removing objects
    
    static func removeAllObjects() {
        guard let managedObjectContext = CoreDataManager.sharedInstance.managedObjectContext else {
            return
        }
        
        removeAllObjects(inContext :managedObjectContext)
    }
    
    static func removeAllObjects(inContext context: NSManagedObjectContext) {
        let allObjects = fetchObjectList(withPredicate: nil, sortDescriptors: nil, fetchLimit: noFetchLimit,
                                         fetchOffset: defaultFetchOffset, fetchBatchSize: defaultFetchBatchSize,
                                         inContext: context)
        guard let objectList = allObjects else {
            return
        }
        
        for object in objectList {
            remove(object: object, inContext: context)
        }
        
        CoreDataManager.sharedInstance.save(context: context)
    }
    
    static func remove(object managedObject: Self) {
        guard let managedObjectContext = CoreDataManager.sharedInstance.managedObjectContext else {
            return
        }
        
        remove(object: managedObject, inContext:managedObjectContext)
    }
    
    static func remove(object managedObject: Self, inContext context: NSManagedObjectContext) {
        context.performAndWait {
            context.delete(managedObject)
        }
    }
    
    // MARK: - Private methods
    
    fileprivate static func assertCondition(for context: NSManagedObjectContext) {
        #if DEBUG
            assert(!(!Thread.isMainThread && context.isEqual(CoreDataManager.sharedInstance.managedObjectContext)))
        #endif
    }
}

extension NSManagedObject: NSManagedObjectEasyAccessProtocol {
}
