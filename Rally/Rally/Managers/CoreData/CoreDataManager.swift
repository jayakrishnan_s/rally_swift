//
//  CoreDataManager.swift
//  Rally
//
//  Created by Jayakrishnan S on 10/10/17.
//  Copyright © 2017 Jayakrishnan.S. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager {
    static let sharedInstance = CoreDataManager()
    internal var dbURL: URL!
    
    // MARK: - Core Data stack
    
    fileprivate init() {
        NotificationCenter.default.addObserver(self, selector: #selector(contextDidSavePrivateQueueContext(_:)),
                                               name: Notification.Name.NSManagedObjectContextDidSave, object: nil)
    }
    
    lazy var managedObjectModel: NSManagedObjectModel? = {
        // The managed object model for the application.
        guard let modelURL = Bundle.main.url(forResource: "Rally", withExtension: "momd") else {
            return nil
        }
        
        return NSManagedObjectModel(contentsOf: modelURL)
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application.
        // This implementation creates and returns a coordinator, having added the store for the application to it.
        guard let managedObjectModel = self.managedObjectModel else {
            return nil
        }
        
        guard let storeURL = self.applicationDocumentsDirectory()?.appendingPathComponent("Rally.sqlite") else {
            return nil
        }
        print("SQLite file Path - " + storeURL.absoluteString)
        
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        
        var automaticMigrationOptions = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: automaticMigrationOptions)
        } catch {
            // Report any error we got.
            var userInfo: [String : Any] = [:]
            userInfo[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            userInfo[NSLocalizedFailureReasonErrorKey] = "There was an error creating or loading the application's saved data."
            userInfo[NSUnderlyingErrorKey] = error
            let error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: userInfo)
            print("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
        self.dbURL = storeURL
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
        
        guard let coordinator = self.persistentStoreCoordinator else {
            return nil
        }
        
        let moc = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        moc.persistentStoreCoordinator = coordinator
        return moc
    }()
    
    lazy var apiManagedObjectContext: NSManagedObjectContext = {
        return self.getNewPrivateManagedObjectContext()
    }()
    
    // MARK: - Private methods
    
    @objc func contextDidSavePrivateQueueContext(_ notification: Notification) {
        guard let savedContext = notification.object as? NSManagedObjectContext else {
            return
        }
        
        if savedContext.persistentStoreCoordinator === self.managedObjectContext?.persistentStoreCoordinator {
            let persistentContexts = [managedObjectContext, apiManagedObjectContext]
            
            for context in persistentContexts where context !== savedContext {
                context?.perform({
                    context?.mergeChanges(fromContextDidSave: notification)
                })
            }
        }
    }
    
    func assertCondition(for context: NSManagedObjectContext) {
        #if DEBUG
            assert(!(!Thread.isMainThread && context.isEqual(self.managedObjectContext)))
        #endif
    }
    
    func applicationDocumentsDirectory() -> URL? {
        // The directory the application uses to store the Core Data store file.
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
    }
    
    // MARK: - Public methods
    
    func save(asynchronously async: Bool = true) {
        guard let managedObjectContext = managedObjectContext else {
            return
        }
        
        self.save(context: managedObjectContext, asynchronously: async)
    }
    
    func save(context objContext: NSManagedObjectContext, asynchronously async: Bool = true) {
        self.assertCondition(for: objContext)
        
        if !objContext.hasChanges {
            return
        }
        
        func saveChanges() {
            do {
                try objContext.obtainPermanentIDs(for: objContext.insertedObjects.map { $0 })
            } catch {
                print("Unresolved error \(error), \(error.localizedDescription)")
            }
            
            do {
                try objContext.save()
            } catch {
                print("Unresolved error on saving context: \(error), \(error.localizedDescription)")
            }
        }
        
        if async {
            objContext.perform({
                saveChanges()
            })
        } else {
            objContext.performAndWait({
                saveChanges()
            })
        }
    }
    
    func getNewPrivateManagedObjectContext() -> NSManagedObjectContext {
        let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateContext.performAndWait {
            privateContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
            privateContext.persistentStoreCoordinator = self.persistentStoreCoordinator
        }
        
        return privateContext
    }
    
    func addStoreCoordinator(_ storeType: String, configuration: String?, storeURL: URL) throws {
        let automaticMigrationOptions = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
        try persistentStoreCoordinator?.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: dbURL, options: automaticMigrationOptions)
    }
}

// MARK: - CoreDataStack (Removing Data)

internal extension CoreDataManager {
    
    func dropAllData() throws {
        // delete all the objects in the db. This won't delete the files, it will
        // just leave empty tables.
        try persistentStoreCoordinator?.destroyPersistentStore(at: dbURL, ofType: NSSQLiteStoreType, options: nil)
        try addStoreCoordinator(NSSQLiteStoreType, configuration: nil, storeURL: dbURL)
    }
}

extension NSManagedObjectContext{
    @discardableResult func saveMe()-> String?{
        do{
            try self.save()
            return nil
        }catch{
            return error.localizedDescription
        }
    }
}
